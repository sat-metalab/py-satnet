# from abc import ABCMeta, abstractmethod
# from typing import Generic, TypeVar, TYPE_CHECKING
#
# from satnet.session import Session
#
# if TYPE_CHECKING:
#     from .message import Message
#
# T = TypeVar('T', bound=Session)
#
#
# class Middleware(Generic[T], metaclass=ABCMeta):
#     """
#     A middleware is a class instance added to a NetBase (either Server or Client) that can intercept
#     incoming messages, process them and also possibly stop further processing of the message by other
#     middleware or the main application/delegate.
#     """
#
#     def __init__(self) -> None:
#         """
#         This is just there for the sake of subclasses that would call `super().__init__()`
#         """
#         pass
#
#     @abstractmethod
#     def handle(self, session: T, message: 'Message') -> bool:
#         """
#         Handle message, if needed, and return True or False
#         depending on if we consider message processing should continue or stop
#
#         :param session: T
#         :param message: Message
#         :return: bool
#         """
#         raise NotImplementedError('users must define `handle(self, session: T, message: Message) -> bool` to use this base class')
