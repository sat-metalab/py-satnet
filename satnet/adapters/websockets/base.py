import asyncio
import logging
from abc import ABCMeta
from typing import Any, Dict, Callable, Optional, TYPE_CHECKING

import websockets  # type: ignore
from websockets import WebSocketCommonProtocol  # type: ignore

from satlib.utils import debug_utils
from satnet import HEX_DUMP
from satnet.base import BaseAdapter
from satnet.errors import ParsingError
from satnet.message import Message

if TYPE_CHECKING:
    from satnet.message import MessageParser

logger = logging.getLogger(__name__)


class WSBase(BaseAdapter, metaclass=ABCMeta):
    """
    Base Websockets Network Adapter
    This provides abstract methods and shared logic for websockets network adapter
    """

    def __init__(self, config: Optional[Dict[str, Any]] = None, message_parser: Optional['MessageParser'] = None) -> None:
        super().__init__(config=config, message_parser=message_parser)

        if not self._use_threads:
            logger.error("Adapter does not support running in the main thread, bypassing setting.")
            self._use_threads = True

        self._ev_loop = asyncio.new_event_loop()

    async def consumer_handler(self, websocket: WebSocketCommonProtocol,
                               process_callback: Optional[Callable[[Message], None]] = None) -> None:
        """
        Coroutine managing incoming messages from the websocket
        :param websocket: websocket object on which to wait for a new message
        :param process_callback: callback to execute after successfully having processed an incoming message
        :return:
        """
        run = True
        while run:
            run = await self._consumer_handler(websocket, process_callback)

    async def _consumer_handler(self, websocket: WebSocketCommonProtocol,
                                process_callback: Optional[Callable[[Message], None]] = None) -> bool:
        try:
            data = await websocket.recv()
        except websockets.ConnectionClosed as e:
            logger.warning(e)
            return False
        else:
            try:
                message = self._message_parser.from_bytes(data)
                if HEX_DUMP:
                    logger.debug(debug_utils.bytes_to_hex(data))
            except ParsingError as e:
                logger.error(str(e))
            else:
                if process_callback:
                    process_callback(message)
                self._network_incoming.append(message)
        return True

    async def producer_handler(self, websocket: WebSocketCommonProtocol) -> None:
        """
        Coroutine managing the server's outgoing messages and sending then through the websocket
        :param websocket:  websocket object on which to wait for a new message
        :return:
        """
        run = True
        while run:
            run = await self._producer_handler(websocket)
            # Necessary to yield from the coroutine otherwise we block the websocket
            await asyncio.sleep(0.001)

    async def _producer_handler(self, websocket: WebSocketCommonProtocol) -> bool:
        """
        Coroutine managing the server's outgoing messages and sending then through the websocket
        :param websocket:  websocket object on which to wait for a new message
        :return:
        """
        for msg in self.outgoing_messages():
            try:
                data = self._message_parser.to_bytes(msg)
                if HEX_DUMP:
                    logger.debug(debug_utils.bytes_to_hex(data))
            except ParsingError as e:
                logger.error(str(e))
            else:
                try:
                    await websocket.send(data)
                except websockets.ConnectionClosed as e:
                    logger.warning(e)
                    return False
        return True

    def start(self) -> None:
        logger.info("Initializing...")
        logger.info("Running!")
        asyncio.set_event_loop(self._ev_loop)
        self.run(0.0, 0.0)  # now and dt are unused in this implementation
        logger.info("Thread exited!")

    def stop(self) -> None:
        logger.info("Closing event loop...")
        asyncio.set_event_loop(self._ev_loop)
        asyncio.get_event_loop().stop()
        asyncio.get_event_loop().close()
        logger.info("Event loop closed!")
