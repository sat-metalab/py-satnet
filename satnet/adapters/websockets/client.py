import asyncio
import logging
from typing import Any, Dict, Optional, TYPE_CHECKING

import websockets  # type: ignore
from websockets import WebSocketCommonProtocol  # type: ignore

from satnet.client import ClientAdapter
from satnet.message import InternalMessageId
from .base import WSBase

if TYPE_CHECKING:
    from satnet.message import MessageParser

logger = logging.getLogger(__name__)


class WSClient(WSBase, ClientAdapter):
    """
    Websockets Client Network Adapter
    """

    def __init__(self, message_parser: Optional['MessageParser'] = None, config: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(message_parser=message_parser, config=config)
        self._websocket = None  # type: Optional[WebSocketCommonProtocol]

    def stop(self) -> None:
        logger.debug("Closing client connection")
        super().stop()

    # region Net Thread

    def start(self) -> None:
        super().start()

    async def handler(self) -> None:
        """
        Handler coroutine launching the consumer and producer tasks
        :return:
        """
        self._internal_incoming.append((InternalMessageId.SESSION_CONNECTED, None))

        consumer_task = asyncio.ensure_future(self.consumer_handler(self._websocket))
        producer_task = asyncio.ensure_future(self.producer_handler(self._websocket))
        try:
            _, pending = await asyncio.wait(
                [consumer_task, producer_task],
                return_when=asyncio.FIRST_COMPLETED,
            )
        except websockets.ConnectionClosed as e:
            logger.warning(e)
        else:
            for task in pending:
                task.cancel()
        finally:
            self._internal_incoming.append((InternalMessageId.SESSION_DISCONNECTED, None))

    async def connect(self) -> None:
        try:
            async with websockets.connect('ws://{}:{}'.format(self._host, self._port), max_size=None) as websocket:
                self._websocket = websocket
                self._internal_incoming.append((InternalMessageId.CONNECTED, None))
                await self.handler()
        except ConnectionResetError as e:
            logger.error(str(e))
        except ConnectionRefusedError as e:
            logger.error(str(e))

    def run(self, now: float, dt: float) -> None:
        asyncio.get_event_loop().run_until_complete(self.connect())

    def _on_internal_message(self, message_id: InternalMessageId, payload: Any) -> None:
        pass

    # endregion
    ...
