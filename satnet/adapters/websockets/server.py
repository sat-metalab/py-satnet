import asyncio
import logging
from typing import Any, Dict, Optional, TYPE_CHECKING

import websockets  # type: ignore
from websockets import WebSocketCommonProtocol  # type: ignore

from satlib.utils.network import get_default_gateway_linux

from satnet.message import InternalMessageId, Message
from satnet.server import ServerAdapter, SessionAdapter
from .base import WSBase

if TYPE_CHECKING:
    from satnet.message import MessageParser

logger = logging.getLogger(__name__)


class WSConnection(SessionAdapter):
    """
    Websockets Remote Session Adapter
    """

    def __init__(self, server: 'WSServer') -> None:
        """
        Create a websockets server remote session
        See `SessionAdapter` for more information about arguments.

        :param server: WSServer
        """
        super().__init__(server=server)

    # region Properties

    def step(self, now: float, dt: float) -> None:
        pass

    @property
    def server(self) -> 'WSServer':
        """
        "Cast" server instance

        :return: WSServer
        """
        assert isinstance(self._server, WSServer)
        return self._server

        # endregion


class WSServer(WSBase, ServerAdapter[WSConnection]):
    """
    Websockets Server Network Adapter
    """

    def __init__(self, message_parser: Optional['MessageParser'] = None, config: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(message_parser=message_parser, config=config)
        self._ws_connections = {}  # type: Dict[bytes, WSConnection]

    async def handler(self, websocket: WebSocketCommonProtocol, uri: str) -> None:
        """
        Handler coroutine launching the consumer and producer tasks and creating the adapter connection
        :param websocket: client websocket object
        :param uri: Unused but necessary for websockets server callback
        :return:
        """
        connection = WSConnection(server=self)
        self._session_adapters[connection.id] = connection
        self._internal_incoming.append((InternalMessageId.SESSION_CONNECTED, connection))

        def set_session_id(msg: Message) -> None:
            msg.session_id = connection.id
        consumer_task = asyncio.ensure_future(
            self.consumer_handler(websocket, set_session_id))
        producer_task = asyncio.ensure_future(self.producer_handler(websocket))
        try:
            _, pending = await asyncio.wait(
                [consumer_task, producer_task],
                return_when=asyncio.FIRST_COMPLETED,
            )
        except websockets.ConnectionClosed as e:
            if connection:
                del self._session_adapters[connection.id]
            logger.warning(e)
        else:
            for task in pending:
                task.cancel()
        finally:
            self._internal_incoming.append((InternalMessageId.SESSION_DISCONNECTED, connection))

    def run(self, now: float, dt: float) -> None:
        # It will only be served locally if we cannot detect the default IP address
        fut = websockets.serve(self.handler, get_default_gateway_linux(), self._port, max_size=None)
        asyncio.get_event_loop().run_until_complete(fut)
        # we need to run forever to keep the connection alive
        asyncio.get_event_loop().run_forever()

    def stop(self) -> None:
        asyncio.get_event_loop().stop()
        asyncio.get_event_loop().close()

    def _on_internal_message(self, message_id: InternalMessageId, payload: Any) -> None:
        pass
