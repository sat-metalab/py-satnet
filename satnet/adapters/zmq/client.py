import logging
import time
from typing import Any, Dict, List, Optional, TYPE_CHECKING, Tuple

import zmq  # type: ignore
from zmq import Again, Context, Socket

from satlib.utils import debug_utils
from satnet import HEX_DUMP
from satnet.client import ClientAdapter
from satnet.errors import ParsingError
from satnet.message import InternalMessageId, Message
from .base import ZMQBase, ZMQFrameType

if TYPE_CHECKING:
    from satnet.message import MessageParser

logger = logging.getLogger(__name__)


class ZMQClient(ZMQBase, ClientAdapter):
    """
    ZMQ Client Network Adapter
    """

    def __init__(
            self,
            message_parser: Optional['MessageParser'] = None,
            config: Optional[Dict[str, Any]] = None,
    ) -> None:
        super().__init__(message_parser=message_parser, config=config)
        self._connected = False
        self._last_received_heartbeat = 0.00
        self._last_sent_heartbeat = 0.00

    def stop(self) -> None:
        logger.debug("Closing client connection")
        super().stop()
        self._connected = False

    # region Net Thread

    def start(self) -> None:
        super().start()
        # Super runs a loop, so once the loop exits, tell the server we disconnected
        self._internal_incoming.append((InternalMessageId.DISCONNECTED, None))

    def _get_socket(self, context: Context) -> Socket:
        socket = context.socket(zmq.DEALER)
        socket.setsockopt(zmq.LINGER, 0)
        socket.setsockopt(zmq.SNDHWM, 0)
        socket.setsockopt(zmq.RCVHWM, 0)
        socket.connect("tcp://{}:{}".format(self._host, self._port))
        return socket

    def _wrap(self, message: Message) -> Tuple[bytes, ...]:
        # Using a DEALER socket requires us to start with an empty frame.
        # Followed by a custom message type encoded by the message parser

        # Count any outgoing message as a heartbeat
        self._last_sent_heartbeat = time.time()

        data = bytes([ZMQFrameType.CUSTOM.value]) + self._message_parser.to_bytes(message)

        logger.debug("Sending message")
        if HEX_DUMP:
            logger.debug(debug_utils.bytes_to_hex(data))

        return b'', data

    def _unwrap(self, msg: List[bytes]) -> None:
        # Using a DEALER socket means every received message starts with an empty frame
        empty, data = msg

        # Record heartbeat
        # Every received message counts as an "I'm alive!" from the server
        # TODO: Move out of `_unwrap`, it is lower level than message unwrapping
        self._last_received_heartbeat = time.time()
        if not self._connected:
            # If we receive a message and are considered disconnected, this is where
            # we change status and consider ourselves connected to the server since ZMQ is connectionless
            self._connected = True
            self._internal_incoming.append((InternalMessageId.CONNECTED, None))

        # Header, message type
        msg_type = ZMQFrameType(int(data[0]))

        # Handle depending on type
        if msg_type is ZMQFrameType.CUSTOM:
            # Place in incoming message queue
            logger.debug("Received message")
            if HEX_DUMP:
                logger.debug(debug_utils.bytes_to_hex(data))
            try:
                message = self._message_parser.from_bytes(data, 1)
            except ParsingError as e:
                logger.error(str(e))
                return
            self._network_incoming.append(message)

        elif msg_type is ZMQFrameType.HEARTBEAT:
            # Internal heartbeat message, we can ignore is only serves as keeping the connection alive
            logger.debug("Heartbeat received!")

        elif msg_type is ZMQFrameType.CONNECTION_CLOSED:
            # Connection closed by peer
            logger.warning("Connection closed by peer!")
            self.stop()

        else:
            logger.warning("Unknown ZMQ message type \"{}\"".format(msg_type))

    def _run(self, now: float, dt: float) -> None:
        if self._last_received_heartbeat == 0.00:
            # Initialize heartbeat, otherwise it will try to connect forever
            # since we'll always be comparing against a last received heartbeat from 1970 ;)
            self._last_received_heartbeat = now

        elif now - self._last_received_heartbeat >= self.connection_timeout:
            # Check for disconnection
            logger.warning("Disconnecting due to inactivity")
            self.stop()

        elif now - self._last_sent_heartbeat >= self.heartbeat_interval:
            # Send heartbeats
            logger.debug("Heartbeat sent!")
            try:
                self._socket.send_multipart(
                    (b'', bytes([ZMQFrameType.HEARTBEAT.value])),
                    zmq.DONTWAIT
                )
            except Again:
                logger.error("ZMQ send queue is full!")
                self.stop()
            self._last_sent_heartbeat = now

    def _on_internal_message(self, message_id: InternalMessageId, payload: Any) -> None:
        pass

    # endregion
