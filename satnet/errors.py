class ParsingError(Exception):
    """
    Error used in the parsing of messages
    """
    pass
