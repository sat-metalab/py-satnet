import uuid
from typing import Callable, List, Optional
from uuid import UUID

from satnet import SerializablePrefix
from satnet.serialization import CustomSerializer, Serializable, serializable, serializer


@serializer(code=0x40)
class UUIDSerializer(CustomSerializer[UUID]):
    """
    Serializer for UUIDs
    Uses the UUID's bytes, otherwise it would serialize as a string which is longer.
    """

    def serialize(self, obj: UUID) -> Optional[bytes]:
        if not isinstance(obj, UUID):
            return None
        return obj.bytes

    def deserialize(self, data: bytes) -> UUID:
        return UUID(bytes=data)


def entity(id: int, fields: Optional[List[str]] = None) -> Callable:
    """
    Helper decorator just for semantics and also adding the prefix ;)
    This is just wrapping the serializable decorator
    """

    return serializable(SerializablePrefix.ENTITY, id, fields)


class Entity(Serializable):
    """
    Entities are serializable objects that can be used when synchronizing objects that need to be referenced with a
    unique id between the server and all the clients.
    """

    _fields = ['_uuid']

    def __init__(self) -> None:
        super().__init__()
        self._ready = False
        self._uuid = uuid.uuid4()

    @property
    def uuid(self) -> UUID:
        return self._uuid

    def ready(self) -> None:
        """
        Ready method
        Call this when the entity has access to its parent
        and should be ready to be used. Like `initialized`
        in Serializable is used to initialize an object when
        it has all of its internal data available, this is used
        to initialize the object when it has access to a working
        application.
        :return: None
        """
        self._ready = True
