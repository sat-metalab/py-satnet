from abc import ABCMeta, abstractmethod
from typing import TypeVar, Generic, Callable, List, Optional, TYPE_CHECKING
from uuid import UUID

from satnet import SerializablePrefix
from satnet.serialization import Serializable, serializable

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from satnet.server import RemoteSession


def action(id: int, fields: Optional[List[str]] = None) -> Callable:
    """
    Helper decorator just for semantics and also adding the prefix ;)
    This is just wrapping the serializable decorator
    """
    return serializable(SerializablePrefix.ACTION, id, fields)


T = TypeVar('T', bound='RemoteSession')


class Action(Generic[T], Serializable):
    """
    Abstract Base Class for actions
    """

    _fields = ['_context']

    def __init__(self, context: Optional[UUID] = None) -> None:
        super().__init__()
        self._context = context

    def __str__(self) -> str:
        return self.__class__.__name__

    @property
    def context(self) -> Optional[UUID]:
        return self._context

    def on_sending(self, session: T) -> None:
        """
        Called when the action is about to be queued to be sent on the network

        :param session: S
        :return: None
        """

    @abstractmethod
    def apply(self, session: T) -> None:
        """
        Apply the action to the session

        :param session: T - Session instance on which to apply the action
        :return: None
        """
        raise NotImplementedError('users must define `def apply(self, session: T) -> None` to use this base class')

    @abstractmethod
    def revert(self, session: T) -> None:
        """
        Revert the action on the session

        :param session: T - Session instance on which to revert the action
        :return: None
        """
        raise NotImplementedError('users must define `def revert(self, session: T) -> None` to use this base class')


@action(id=0x01)
class Transaction(Action[T]):
    """
    Transaction action
    Takes a list of actions and applies/reverts them in order
    """

    # TODO: Manage context for the transaction

    _fields = ['_actions']

    def __init__(self, actions: List[Action[T]] = list()) -> None:
        """
        Create a transaction action

        :param actions: List[Action[T]] - List of actions to include in the transaction
        :return: None
        """
        super().__init__()
        self._actions = actions

    def apply(self, session: T) -> None:
        """
        Applies the actions in their list order

        :param session: T - Session instance on which to apply the actions
        :return: None
        """
        for cmd in self._actions:
            cmd.apply(session=session)

    def revert(self, session: T) -> None:
        """
        Reverts the actions in the reversed list order

        :param session: T - Session instance on which to revert the action
        :return: None
        """
        for cmd in reversed(self._actions):
            cmd.revert(session=session)


class ActionHistory(Generic[T]):
    """
    Action History
    Manages a list of actions in order to provide undo/redo functionality
    A action history uses a session to apply action to, it is used as a form of dependency injection
    for actions to act on an instance of a controllable object
    """

    _history: List[Action[T]] = list()
    _pointer: int = 0

    def __init__(self) -> None:
        pass

    def apply(self, action: Action[T], session: T) -> None:
        """
        Apply a action to the session and adds it to the stack

        :param action: Action[T] - Action to apply
        :param session: T - Session instance on which to apply the action
        :return: None
        """
        action.apply(session=session)

        # If we're in a point where we came back in the history,
        # remove redoable actions from this point on since we are breaking
        # the chain by inserting a new action in the history
        del self._history[self._pointer:]

        self._history.append(action)
        self._pointer = len(self._history)

    def undo(self, session: T) -> None:
        """
        Undo the last applied action, if any.
        We don't remove the action from the history just yet as we can also redo actions
        unless a new one is applied and overwrite the redo stack.

        :param session: T - Session instance on which to revert the action
        :return: None
        """
        if self._pointer > 0:
            self._pointer -= 1
            self._history[self._pointer].revert(session=session)

    def redo(self, session: T) -> None:
        """
        Redo the last undone action, if any.
        If a new action is applied after an undo it will overwrite the redo stack.

        :param session: T - Session instance on which to revert the action
        :return: None
        """
        if self._pointer < len(self._history):
            self._history[self._pointer].apply(session=session)
            self._pointer += 1

    def reset(self) -> None:
        """
        Reset the action history

        :return: None
        """
        del self._history[:]
        self._pointer = 0
