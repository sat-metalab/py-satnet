import logging
from abc import ABCMeta, abstractmethod
from collections import deque
from threading import Thread
from typing import Any, Deque, Dict, Generator, Optional, TYPE_CHECKING, Tuple

from satlib.utils.config_utils import defaults
# from satnet.middleware import Middleware
from satnet.message import InternalMessageId, Message, MessageParser
from satnet.request import Request

logger = logging.getLogger(__name__)

# Make sure we import all serializable classes we might use. They should be imported even if not directly used in code
# because a request might be coming from the network and we should be able to deserialize and handle it.
# noinspection PyUnresolvedReferences
import satnet.commands

defaultNetworkConfig: Dict[str, Any] = {}
defaultAdapterConfig: Dict[str, Any] = {
    'use_threads': True
}


class BaseAdapter(metaclass=ABCMeta):
    """
    Base Network Adapter
    This provides abstract methods and shared logic for network adapters
    """

    def __init__(
            self,
            config: Optional[Dict[str, Any]] = None,
            message_parser: Optional[MessageParser] = None,
    ) -> None:
        """
        The default message parser will simply parse messages without any custom protocol headers, pass your own custom
        message parser to the constructor if your application needs to either wrap or reimplement de default parser.

        :param config: Optional[Dict[str, Any]] - Configuration
        :param message_parser: Optional[MessageParser]
        """

        assert config is None or isinstance(config, dict)

        self._config = defaults(defaultAdapterConfig, config)
        self._message_parser = message_parser if message_parser else MessageParser()
        self._use_threads: bool = self._config.get('use_threads')
        self._internal_incoming: Deque[Tuple[InternalMessageId, Optional[Any]]] = deque()
        self._internal_outgoing: Deque[Tuple[InternalMessageId, Optional[Any]]] = deque()
        self._network_incoming: Deque[Message] = deque()
        self._network_outgoing: Deque[Message] = deque()

        self._started = False

    @property
    def use_threads(self) -> bool:
        return self._use_threads

    def start(self) -> None:
        """
        Start the adapter

        If use_threads is True, this method will be run as a thread by NetBase,
        in that case it should not return unless terminated.

        If use_threads is False, this method should initialize the adapter.

        :return: None
        """
        self._started = True

    def stop(self) -> None:
        """
        Stop the adapter

        If use_threads is True, this method should provide a way for NetBase to
        terminate the thread and cleanup.

        If use_threads is False, this method should only do the cleanup.

        :return: None
        """
        self._started = False

    def step(self, now: float, dt: float) -> None:
        if self._started and not self._use_threads:
            # Only run on step if we are not using threads
            self.run(now=now, dt=dt)

    @abstractmethod
    def run(self, now: float, dt: float) -> None:
        """
        Run loop, separated from start/step for testing and threads/no threads implementations
        :return: None
        """

    def send(self, message: Message) -> None:
        """
        Helper method to queue a message to be sent

        :param message: Message
        :return: None
        """
        self._network_outgoing.append(message)

    def incoming_messages(self) -> Generator[Message, None, None]:
        """
        Generator yielding incoming network messages

        :return: Generator[Message, None, None]
        """
        while True:
            try:
                yield self._network_incoming.popleft()
            except IndexError:
                break

    def outgoing_messages(self) -> Generator[Message, None, None]:
        """
        Generator yielding outgoing network messages

        :return: Generator[Message, None, None]
        """
        while True:
            try:
                yield self._network_outgoing.popleft()
            except IndexError:
                break

    def send_internal(self, message: Tuple[InternalMessageId, Any]) -> None:
        """
        Helper method to queue an internal message to be sent

        :param message: Tuple[InternalMessageId, Any]
        :return: None
        """
        self._internal_outgoing.append(message)

    def incoming_internal_messages(self) -> Generator[Tuple[InternalMessageId, Any], None, None]:
        """
        Generator yielding incoming internal messages

        :return: Generator[Tuple[InternalMessageId, Any], None, None]
        """
        while True:
            try:
                yield self._internal_incoming.popleft()
            except IndexError:
                break

    def outgoing_internal_messages(self) -> Generator[Tuple[InternalMessageId, Any], None, None]:
        """
        Generator yielding outgoing internal messages

        :return: Generator[Tuple[InternalMessageId, Any], None, None]
        """
        while True:
            try:
                yield self._internal_outgoing.popleft()
            except IndexError:
                break

    @abstractmethod
    def _on_internal_message(self, message_id: InternalMessageId, payload: Any) -> None:
        """
        Internal message handler
        This method should be implemented in order to receive incoming internal messages

        :param message_id: InternalMessageId
        :param payload: Any
        :return: None`
        """
        raise NotImplementedError(
            'users must define `def _on_internal_message(self, message_id: InternalMessageId, payload: bytes) -> None` to use this base class')


class NetBase(metaclass=ABCMeta):
    """
    Network Base
    This is shared by Server and Client and provides the common logic and
    abstract stubs that they both have to implement.
    """

    def __init__(self, adapter: BaseAdapter, config: Optional[Dict[str, Any]] = None) -> None:
        """
        The target is used to give network subclasses and callbacks a context upon which to act.
        It should be seen as the object for which we are implementing networking.

        The adapter is the network adapter implementation to use for network communications.
        NetBase and its subclasses should not use sockets or any network-related library directly,
        they should only exchange messages with the adapters.

        :param config: Optional[Dict[str, Any]] - Network configuration
        :param adapter: BaseAdapter - Network adapter
        """
        assert config is None or isinstance(config, dict)
        assert isinstance(adapter, BaseAdapter)

        self._config = defaults(defaultNetworkConfig, config)
        self._adapter = adapter
        self._net_thread: Optional[Thread] = None
        self._requests: Dict[int, Request] = {}

    # region Properties

    @property
    def config(self) -> Dict[str, Any]:
        return self._config

    @property
    def requests(self) -> Dict[int, Request]:
        return self._requests

    # endregion

    # region Lifecycle

    def _run(self) -> None:
        """
        Run the network thread

        :return: None
        """

        logger.debug("Starting network adapter...")
        if self._adapter.use_threads:
            self._net_thread = Thread(target=self._adapter.start, daemon=True, name="Network Adapter")
            self._net_thread.start()
        else:
            self._adapter.start()

    def _close(self) -> None:
        """
        Stop the network adapter

        :return: None
        """
        logger.debug("Stopping network adapter...")
        self._adapter.stop()
        self._close_connection()

    def _close_connection(self) -> None:
        """
        Close and join the network thread if needed

        :return: None
        """

        if self._net_thread:
            logger.debug("Waiting for network thread to join...")
            self._net_thread.join()
            logger.debug("Done!")

    def step(self, now: float, dt: float) -> None:
        """
        Step, in the main thread
        This should be called in your main loop in order to process incoming messages.

        :param now: float - Time
        :param dt: float - Delta time since last step
        :return: None
        """

        self._adapter.step(now=now, dt=dt)

        # Read incoming internal messages first
        for message_id, message_payload in self._adapter.incoming_internal_messages():
            self._on_internal_message(message_id, message_payload)

        # Read incoming messages
        for message in self._adapter.incoming_messages():
            self._on_message(message)
            # # First pass the message to registered middleware
            # stopped = False
            # for middleware in self._middleware:
            #     # Middleware can prevent further message handling by returning True
            #     stopped = not middleware.handle(self._target, self, message)
            #     if stopped:
            #         break
            # if not stopped:
            #     # Send to subclass message handler
            #     self._on_message(message)

    # endregion

    # region Methods

    # def add_middleware(self, middleware: Middleware) -> None:
    #     """
    #     Add a message handling middleware
    #
    #     :param middleware: Middleware
    #     :return: None
    #     """
    #     assert isinstance(middleware, Middleware)
    #     self._middleware.append(middleware)

    def send(self, message: Message) -> None:
        """
        Send message
        :param message:
        :return:
        """
        assert isinstance(message, Message)
        self._adapter.send(message)

    def get_request(self, request: Request) -> Message:
        """
        Get a request message
        Requests should be acquired through this method in order to be able to receive a response.
        Creating request messages manually will not keep them in memory for when the response arrives.

        :param request: Request
        :return: None
        """
        assert isinstance(request, Request)
        self._requests[request.id] = request
        return Message(data=request)

    # endregion

    # region Handlers

    @abstractmethod
    def _on_message(self, message: Message) -> None:
        """
        Message handler.
        This method should be implemented in order to receive incoming messages

        :param message: Message
        :return: None
        """
        raise NotImplementedError(
            'users must define `def _on_message(self, message: Message) -> None` to use this base class')

    @abstractmethod
    def _on_internal_message(self, message_id: InternalMessageId, payload: Any) -> None:
        """
        Internal message handler
        This method should be implemented in order to receive incoming internal messages

        :param message_id: InternalMessageId
        :param payload: Any
        :return: None
        """
        raise NotImplementedError(
            'users must define `def _on_internal_message(self, message_id: InternalMessageId, payload: bytes) -> None` to use this base class')

    # endregion
    ...
