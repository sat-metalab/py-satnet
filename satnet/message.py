import logging
from typing import TYPE_CHECKING
from enum import IntEnum, unique
from typing import Optional

from satnet.serialization import Serializable
from .errors import ParsingError

if TYPE_CHECKING:
    from satnet.session import Session

logger = logging.getLogger(__name__)


@unique
class MessageDataType(IntEnum):
    """
    Message Data Type
    A message can contain data in different formats, so this is used to differentiate them.
    """
    RAW = 0x01  # Raw bytes
    SERIALIZED = 0x02  # msgpack serialized data


@unique
class InternalMessageId(IntEnum):
    """
    Internal Message Id
    Id used to communicate messages between the network adapters and net classes
    """
    CONNECTED = 0x01
    DISCONNECTED = 0x02
    DISCONNECT = 0x03
    SESSION_CONNECTED = 0x04
    SESSION_DISCONNECTED = 0x05
    SESSION_DISCONNECT = 0x06


class Message:
    """
    Message Class
    """

    def __init__(
            self,
            data: Optional[Serializable] = None,
            raw: Optional[bytes] = None,
            session: Optional['Session'] = None
    ) -> None:
        """
        Create a message instance

        :param data: Serializable - Object to serialize (when sending) or deserialized object (when receiving)
        :param raw: bytes - Raw data to deserialize (when receiving) or raw data to send (when sending)
        :param session: 'RemoteSession' - Remote session, used when sending to know where to send the message or when receiving to tell where it came from
        """
        self.data = data
        self.raw = raw
        self.session = session
        self.session_id: Optional[int] = None


class MessageParser:
    """
    Basic Message Parser
    The default message parser will simply parse messages without any custom protocol headers,
    implement your own custom message parser if your application needs to either wrap or redefine de default protocol.
    """

    @classmethod
    def from_bytes(cls, data: bytes, offset: int = 0) -> Message:
        """
        Parses bytes into a Message instance

        :param data: bytes
        :param offset: int - Offset in the data where to start looking for a message
        :return: Optional[Message]
        """

        length = len(data) - offset
        if length < 1:
            raise ParsingError("Message too short")

        # With payload, find out which type
        try:
            data_type = MessageDataType(int(data[offset]))
        except ValueError:
            raise ParsingError("Invalid data type \"{}\"".format(int(data[offset])))

        if data_type is MessageDataType.RAW:
            # Raw payload
            return Message(raw=data[offset + 1:])

        elif data_type is MessageDataType.SERIALIZED:
            # Serialized payload
            raw = data[offset + 1:]
            data_len = len(raw)
            return Message(
                raw=raw,
                data=Serializable.deserialize(raw) if data_len > 0 else None,
            )

        else:
            raise ParsingError("Unhandled data type \"{}\"".format(data_type))

    @classmethod
    def to_bytes(cls, message: Message) -> bytes:
        """
        Get the bytes for the message

        :return: bytes
        """

        if message.data:
            payload = bytes([MessageDataType.SERIALIZED.value]) + message.data.serialize()
        elif message.raw:
            payload = bytes([MessageDataType.RAW.value]) + message.raw
        else:
            raise ParsingError("Nothing to encode")

        return payload
