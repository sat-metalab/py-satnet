import logging
import uuid
from abc import ABCMeta, abstractmethod
from typing import Optional, Dict, Set, TYPE_CHECKING
from uuid import UUID

from satnet.command import Command
from satnet.message import Message
from satnet.notification import Notification
from satnet.request import Request

if TYPE_CHECKING:
    from satnet.base import NetBase

logger = logging.getLogger(__name__)


class Session(metaclass=ABCMeta):
    """
    Base session, represents a session either on the client side (local session) or the server side (remote session).
    """

    def __init__(self, net: 'NetBase', id: Optional[int] = None) -> None:
        self._id = id if id is not None else 0
        self._net = net
        self._subscriptions: Dict[int, Set[UUID]] = {}
        # Since they're unique, we can use an arbitrary UUID to specify the wildcard subscription
        self._subscription_wildcard = uuid.uuid4()

    def __str__(self) -> str:
        return "\"{}\"".format(self._id)

    # region Properties

    @property
    def id(self) -> int:
        """
        Session id

        :return: int
        """
        return self._id

    @id.setter
    def id(self, value: int) -> None:
        self._id = value

    @property
    def net(self) -> 'NetBase':
        return self._net

    # endregion

    # region Lifecycle

    def step(self, now: float, dt: float) -> None:
        """
        Step this session instance

        :param now: float - Time
        :param dt: float - Delta time since last step
        :return: None
        """
        pass

    # endregion

    # region Methods

    @abstractmethod
    def disconnect(self) -> None:
        pass

    def subscribe(self, topic: int, context: Optional[UUID] = None) -> None:
        """
        Subscribe the session to a topic

        :param topic: str - Topic to subscribe to
        :param context: uuid.UUID - Context to subscribe to (if applicable, if None, will subscribe to all topic notifications)
        :return: None
        """

        subscription = self._subscriptions.get(topic)
        if subscription is None:
            subscription = set()
            self._subscriptions[topic] = subscription

        subscription.add(context if context else self._subscription_wildcard)

    def unsubscribe(self, topic: int, context: Optional[UUID] = None) -> None:
        """
        Unsubscribe the session from a topic

        :param topic: str - Topic to unsubscribe from
        :param context: uuid.UUID - Context to unsubscribe from (if applicable, if None, will unsubscribe from all topic notifications)
        :return: None
        """

        subscription = self._subscriptions.get(topic)
        if subscription is None:
            logger.warning("Session {} trying to unsubscribe from a non-subscription".format(self))
            return

        if context is None:
            del self._subscriptions[topic]
        else:
            try:
                subscription.remove(context)
            except KeyError:
                logger.warning(
                    "Session {} trying to unsubscribe from a context that was not subscribed to".format(self))
            finally:
                if not subscription:
                    # If nothing remains remove subscription so that it doesn't stay hanging there
                    del self._subscriptions[topic]

    def send(self, message: Message) -> None:
        """
        Send a message
        Simple wrapper around `NetBase.send` that automatically inserts the session in the message

        :param message: Message
        :return: None
        """
        assert isinstance(message, Message)
        message.session = self
        self._net.send(message)

    def command(self, command: Command) -> None:
        """
        Send a command

        :param command: Command
        :return: None
        """
        assert isinstance(command, Command)
        # logger.debug("Sending command: {}".format(command))
        command.on_sending(self)
        self.send(Message(data=command))

    def request(self, request: Request) -> None:
        """
        Make a request to the session's client

        :param request: Request
        :return: None
        """
        assert isinstance(request, Request)
        # logger.debug("Sending request: {}".format(request))
        request.on_sending(self)
        self.send(self._net.get_request(request))

    def notify(self, notification: Notification) -> None:
        """
        Send the notification to the session's client if it has a subscription for it

        :param notification: N
        :return: None
        """
        assert isinstance(notification, Notification)

        subscription = self._subscriptions.get(notification.topic)
        if subscription is None:
            return

        if self._subscription_wildcard in subscription or (notification.context is not None and notification.context in subscription):
            # Notify if either subscribed to the -1 wildcard or the notification has a context we are subscribed to
            # logger.debug("Sending notification: {}".format(notification))
            notification.on_sending(self)
            self.send(Message(data=notification))

    def notify_all(self, notification: Notification) -> None:
        """
        Send the notification to everyone subscribe to it.
        Obviously, on a local session (client) this will only notify the server.

        :param notification: N
        :return: None
        """
        assert isinstance(notification, Notification)
        self.notify(notification)

    def notify_others(self, notification: Notification) -> None:
        """
        Send the notification to everyone subscribe to it BUT the session sending it.
        Obviously, on a local session (client) this is a noop.

        :param notification: N
        :return: None
        """
        assert isinstance(notification, Notification)
        pass

    # endregion

    # region Callbacks

    def on_connecting(self) -> None:
        """
        Called when the client is connecting to the server
        :return:
        """
        logger.debug("Session {} connecting".format(self))

    def on_connected(self) -> None:
        """
        Called when the client is connected to the server
        :return: None
        """
        logger.info("Session {} connected".format(self))

    def on_disconnected(self) -> None:
        """
        Called when the client is disconnected from the server
        :return: None
        """
        logger.info("Session {} disconnected".format(self))

    def on_message(self, message: Message) -> None:
        """
        Called when a message could be be handled internally by the base network class.

        :param message: Message
        :return: None
        """
        pass

        # endregion
