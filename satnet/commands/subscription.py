import logging
import uuid
from typing import Optional, TYPE_CHECKING

from satnet.command import Command, CommandId, builtin_command

if TYPE_CHECKING:
    from satnet.session import Session

logger = logging.getLogger(__name__)


@builtin_command(id=CommandId.SUBSCRIBE)
class SubscribeCommand(Command['Session']):
    _fields = ['topic', 'context']

    def __init__(self, topic: Optional[int] = None, context: Optional[uuid.UUID] = None) -> None:
        super().__init__()
        self.topic = topic
        self.context = context

    def handle(self, session: 'Session') -> None:
        if self.topic:
            logger.info("Subscribing \"{}\" to topic \"{}\"".format(session, self.topic))
            session.subscribe(topic=self.topic, context=self.context)
        else:
            logger.warning("Received a subscription command without a topic from {}".format(session))


@builtin_command(id=CommandId.UNSUBSCRIBE)
class UnsubscribeCommand(Command['Session']):
    _fields = ['topic', 'context']

    def __init__(self, topic: Optional[int] = None, context: Optional[uuid.UUID] = None) -> None:
        super().__init__()
        self.topic = topic
        self.context = context

    def handle(self, session: 'Session') -> None:
        if self.topic:
            logger.info("Unsubscribing \"{}\" from topic \"{}\"".format(session, self.topic))
            session.unsubscribe(topic=self.topic, context=self.context)
        else:
            logger.warning("Received an unsubscription command without a topic from {}".format(session))
