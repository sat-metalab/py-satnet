import logging
import uuid
from typing import Optional, TYPE_CHECKING
from uuid import UUID

from satnet.command import ClientCommand, CommandId, builtin_command

if TYPE_CHECKING:
    from satnet.server import RemoteSession

logger = logging.getLogger(__name__)


@builtin_command(id=CommandId.UNDO)
class UndoCommand(ClientCommand['RemoteSession']):
    _fields = ['context']

    def __init__(self, context: Optional[uuid.UUID] = None) -> None:
        super().__init__()
        self.context = context

    def handle(self, session: 'RemoteSession') -> None:
        context = session.server.get_action_context(self.context)
        if context:
            context.undo(session=session)
        else:
            logger.warning("Received undo for invalid context ({})".format(self.context))


@builtin_command(id=CommandId.REDO)
class RedoCommand(ClientCommand['RemoteSession']):
    _fields = ['context']

    def __init__(self, context: Optional[UUID] = None) -> None:
        super().__init__()
        self.context = context

    def handle(self, session: 'RemoteSession') -> None:
        context = session.server.get_action_context(self.context)
        if context:
            context.redo(session=session)
        else:
            logger.warning("Received redo for invalid context ({})".format(self.context))
