import logging
from abc import ABCMeta, abstractmethod
from collections import OrderedDict
from io import BytesIO
from typing import ClassVar, Dict, List, Callable, Any, Optional, Type, Generic, TypeVar, cast
from sys import version_info

import msgpack  # type: ignore

# GenericMeta appeared in Python 3.6, and disappeared in 3.7
if version_info < (3, 7):
    from typing import GenericMeta
else:
    class GenericMeta(ABCMeta):  # type: ignore
        pass

logger = logging.getLogger(__name__)


def serializable(prefix: int, id: int, fields: Optional[List[str]] = None) -> Callable:
    """
    Decorator used to register serializable classes

    :param prefix: int - Prefix for the ids
    :param id: int - Id of the serializable object, used to identify when serializing/deserializing
    "param fields: Optional[List[str]] - Fields to serialize
    :return:
    """

    def deco(cls: Type[Serializable]) -> Callable:
        cls.serialization_prefix = int(prefix)
        cls.serialization_id = int(id)

        if fields is not None:
            cls._fields = fields if not cls._fields else cls._fields + fields

        # Register the class so that we can find it to create instances when deserializing
        Serializable.register(cls)

        return cls

    return deco


class SerializableABCMeta(GenericMeta):
    """
    Meta class that allows us to call `initialize()` after the `__init__()` has been called.
    This allows us to initialize the class from the deserialized values if need be.
    """

    def __call__(cls, *args: Any, **kwargs: Any) -> Any:
        obj = type.__call__(cls, *args, **kwargs)
        obj.initialize()
        return obj


class Serializable(metaclass=SerializableABCMeta):
    """
    Base class for serializable objects.

    IMPORTANT: Any class deriving from this class must ONLY use the `__init__()` method to initialize fields that will
    be serialized and use the `initialize()` method for the rest. This is because the `__init__()` method will NOT be
    called before deserializing the data, only `initialize()` will be called AFTER the data has been deserialized. In
    manual instantiation both `__init__()` and `initialize()` will be called in that order.
    """
    # TODO: Change previous message once it is established that this method of not calling the constructor is not needed

    _classes: ClassVar[Dict[int, Dict[int, Type['Serializable']]]] = dict()

    serialization_prefix: ClassVar[int] = 0x00
    serialization_id: ClassVar[int] = 0x00
    _fields: ClassVar[List[str]] = []  # Cheating a little bit, it is a class var but MyPy doesn't like us overriding it
    _serializers: ClassVar[Dict[int, 'CustomSerializer']] = {}

    @classmethod
    def add_custom_serializer(cls, code: int, serializer: 'CustomSerializer') -> None:
        if code in cls._serializers:
            raise Exception("Serializer code {} already in use".format(code))
        cls._serializers[code] = serializer

    @classmethod
    def register(cls, serializable_type: Type['Serializable']) -> None:
        """
        Register a Serializable class

        :param serializable_type: Type[Serializable] - Serializable class to register
        :return: None
        """
        if serializable_type.serialization_prefix not in cls._classes:
            cls._classes[serializable_type.serialization_prefix] = dict()
            classes = cls._classes[serializable_type.serialization_prefix]
        else:
            classes = cls._classes[serializable_type.serialization_prefix]
        if serializable_type.serialization_id in classes:
            raise Exception(
                "Class id \"{}\" with prefix \"{}\" already used by \"{}\"".format(
                    serializable_type.serialization_id,
                    serializable_type.serialization_prefix,
                    classes[serializable_type.serialization_id]
                )
            )

        # logger.debug("Registering class \"{}\" with prefix \"{}\" id \"{}\"".format(
        #     serializable_type, serializable_type.serialization_prefix, serializable_type.serialization_id))
        classes[serializable_type.serialization_id] = serializable_type

    @classmethod
    def get_instance(cls, prefix: int, id: int) -> Optional['Serializable']:
        """
        Get an instance of a registered serializable class by prefix and id.

        :param prefix: int - Serializable class registration prefix
        :param id: int - Serializable class registration id
        :return: Optional[Serializable]
        """
        classes = cls._classes.get(prefix)
        if not classes:
            return None
        serializable_type = classes.get(id)
        if not serializable_type:
            return None

        # This is used in order to prevent our post __init__ initialize method
        # from being called, but still call the normal __init__
        obj: Serializable = serializable_type.__new__(serializable_type)
        serializable_type.__init__(obj)

        return obj

    @classmethod
    def ext_hook(cls, code: int, data: bytes) -> Any:
        """
        Ext hook for msgpack.
        Deserializes objects with ext code 0x00

        :param code: int - Ext code
        :param data: bytes - Data to unpack
        :return: Any
        """
        serializer = cls._serializers.get(code)
        if serializer:
            return serializer.deserialize(data)
        return msgpack.ExtType(code, data)

    @classmethod
    def deserialize(cls, data: bytes) -> Optional['Serializable']:
        """
        Deserialize the passed bytes into a class instance
        Will recursively deserialize embedded Serializable objects using msgpack's extended types

        :param data: bytes - Packed bytes to deserialize
        :return: Optional[Serializable]
        """

        if len(data) == 0:
            return None

        with BytesIO(data) as buf:
            unpacker = msgpack.Unpacker(
                buf,
                encoding='utf-8',
                ext_hook=cls.ext_hook,
                max_buffer_size=1 << 31 - 1  # Maximum 32bits signed integer
            )

            # Start by getting prefix and id
            cls_prefix = unpacker.unpack()
            if cls_prefix is None:
                return None

            cls_id = unpacker.unpack()

            # Create a Serializable instance of the packed data type
            instance = cls.get_instance(cls_prefix, cls_id)
            if instance is None:
                logger.warning("Class prefix \"%s\" id \"%s\" not found!", cls_prefix, cls_id)
                return None

            # Then unpack data into the Serializable class instance
            instance._unpack(unpacker.unpack)

            # Then initialize it, the instance we got never had its `__init__()` method called.
            instance.initialize()

            return instance

    def __init__(self) -> None:
        # Enables/disables serialization of this serializable
        self._serialize = True

    def initialize(self) -> None:
        """
        Called after the constructor or after being deserialized in order to use
        the values that came either from the constructor or serialization.
        :return: None
        """
        pass

    @property
    def fields(self) -> List[str]:
        """
        Concatenates all _fields from the class hierarchy, thus removing the need for subclasses to manually retrieve
        and merge the parent's class fields and its own manually.

        :return: Set[str]
        """
        # TODO: Cache/memoize this
        # https://stackoverflow.com/questions/480214/how-do-you-remove-duplicates-from-a-list-in-whilst-preserving-order/39835527#39835527
        return list(OrderedDict.fromkeys([
            field for fields in [
                cast(Type[Serializable], c)._fields for c in reversed(self.__class__.mro()) if hasattr(c, '_fields') and cast(Type[Serializable], c)._fields is not None
            ] for field in fields
        ]))

    def _unpack(self, unpack: Callable) -> None:
        """
        Unpack data, either automatically or manually

        :param unpack: Callable - Method to call to read in the next value
        :return: None
        """

        # Automatic unpacking using the field list
        for field in self.fields:
            setattr(self, field, unpack())

        # Manual unpacking, if needed
        self.unpack(unpack)

    def serialize(self) -> bytes:
        """
        Serialize the class instance into bytes
        Will recursively serialize embedded Serializable objects using msgpack's extended types

        :return: bytes - Serialized bytes
        """

        def default(obj: Any) -> msgpack.ExtType:
            """
            If the obj to pack is a Serializable instance, use ExtType 0x00 with the serialized data.

            :param obj: Any
            :return: msgpack.ExtType
            """
            for code, serializer in self._serializers.items():
                data = serializer.serialize(obj)
                if data:
                    return msgpack.ExtType(code, data)
            raise TypeError("Unknown type encountered: {}".format(obj, self))

        packer = msgpack.Packer(default=default, autoreset=False, use_bin_type=True)
        packer.pack(self.serialization_prefix)
        packer.pack(self.serialization_id)

        # Automatic packing using the fields list
        for field in self.fields:
            try:
                field_value = getattr(self, field)
                if isinstance(field_value, Serializable) and not field_value._serialize:
                    # Skip disabled serializable fields
                    field_value = None

                elif isinstance(field_value, List):
                    # Reconstruct a list without the disabled serializable
                    field_value = [item for item in field_value if not isinstance(item, Serializable) or item._serialize]

                packer.pack(field_value)
            except TypeError as e:
                logger.error("{}, while serializing field \"{}\" of \"{}\"".format(e, field, self))

        # Manual packing, if needed
        self.pack(packer.pack)

        return packer.bytes()

    def pack(self, write: Callable[[Any], None]) -> None:
        """
        Manually pack the class data.
        If implemented, the `unpack` method must also be implemented.

        :param write: Callable - Method to call to write the next packed value
        :return: None
        """
        pass

    def unpack(self, read: Callable[[], Any]) -> None:
        """
        Manually unpack the class data.
        If implemented, the `pack` method must also be implemented.

        :param read: Callable - Method to call to read the next packed value
        :return: None
        """
        pass


T = TypeVar('T')


def serializer(code: int) -> Callable:
    def deco(cls: Type[CustomSerializer]) -> Callable:
        Serializable.add_custom_serializer(code=code, serializer=cls())
        return cls

    return deco


class CustomSerializer(Generic[T], metaclass=ABCMeta):
    @abstractmethod
    def serialize(self, obj: T) -> Optional[bytes]:
        """
        This method should return a bytes representation of obj
        if it is able to serialize it and None if it doesn't
        support the serialization of obj.
        :param obj: T
        :return: Optional[bytes]
        """
        pass

    @abstractmethod
    def deserialize(self, data: bytes) -> Optional[T]:
        """
        This method should return an instance of T from
        the bytes in data or None if there was an error.
        :param data: bytes
        :return: Optional[T]
        """
        pass


@serializer(code=0x00)
class SerializableSerializer(CustomSerializer[Serializable]):
    def serialize(self, obj: Serializable) -> Optional[bytes]:
        if not isinstance(obj, Serializable):
            return None
        return obj.serialize()

    def deserialize(self, data: bytes) -> Optional[Serializable]:
        return Serializable.deserialize(data)
