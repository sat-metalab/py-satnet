import logging
import uuid
from abc import ABCMeta, abstractmethod
from typing import Any, Callable, Dict, Generic, List, Optional, Type, TypeVar, ValuesView
from uuid import UUID
from satnet import SerializablePrefix

from satnet.action import Action, ActionHistory
from satnet.base import BaseAdapter, NetBase
from satnet.command import Command, ServerCommand
from satnet.message import InternalMessageId, Message, MessageParser
from satnet.notification import ClientNotification, Notification, ServerNotification
from satnet.request import ClientRequest, ClientResponse, Response, ServerRequest, ServerResponse
from satnet.session import Session

logger = logging.getLogger(__name__)


class SessionAdapter(metaclass=ABCMeta):
    """
    Session Network Adapter
    """

    _id_counter = -1

    @classmethod
    def get_id(cls) -> int:
        """
        Generate a new pseudo unique session id

        :return: int
        """
        cls._id_counter += 1
        return cls._id_counter

    def __init__(self, server: 'ServerAdapter') -> None:
        assert isinstance(server, ServerAdapter)
        self._server = server
        self._id = self.get_id()

    @property
    def id(self) -> int:
        """
        Session id
        This id is created locally to identify the session internally,
        it has no network meaning whatsoever.

        :return: int
        """
        return self._id

    @abstractmethod
    def step(self, now: float, dt: float) -> None:
        """
        Abstract step method

        :return: None
        """


A = TypeVar('A', bound=SessionAdapter)


class ServerAdapter(Generic[A], BaseAdapter, metaclass=ABCMeta):
    """
    Base Server Network Adapter
    This provides abstract methods and shared logic for server network adapters
    """

    def __init__(
            self,
            config: Optional[Dict[str, Any]] = None,
            message_parser: Optional[MessageParser] = None,
    ) -> None:
        super().__init__(config=config, message_parser=message_parser)
        self._port: int = self._config.get('port')
        self._session_adapters: Dict[int, A] = {}

    # region Properties

    @property
    def port(self) -> int:
        """
        Port to listen for incoming connections

        :return: int
        """
        return self._port

    @port.setter
    def port(self, value: int) -> None:
        self._port = value

    @property
    def session_adapters(self) -> Dict[int, A]:
        """
        List of session adapters
        :return: Dict[int, A]
        """
        return self._session_adapters

    # endregion
    ...


class RemoteSession(Session, metaclass=ABCMeta):
    """
    Class representing a client connected to the server.
    It hold anything related to the server-side state of that client and
    provides helper methods to directly communicate with that client.
    """

    def __init__(
            self,
            server: 'Server',
            id: int
    ) -> None:
        assert isinstance(server, Server)
        super().__init__(net=server, id=id)

    def __str__(self) -> str:
        return str(self._id)

    # region Properties

    @property
    def server(self) -> 'Server':
        assert isinstance(self._net, Server)
        return self._net

    # endregion

    # region Methods

    def disconnect(self) -> None:
        """
        Disconnect this session

        :return: None
        """
        self.server.disconnect(self)

    def notify_all(self, notification: Notification) -> None:
        """
        Send the notification to everyone subscribe to it.
        Remote session override that calls the `notify_all` method from the server.

        :param notification: Notification
        :return: None
        """
        assert isinstance(notification, ServerNotification)
        self.server.notify_all(notification)

    def notify_others(self, notification: Notification) -> None:
        """
        Send the notification to everyone subscribe to it BUT the session sending it.
        Remote session override that calls the `notify_all_excluding` method from the server.

        :param notification: Notification
        :return: None
        """
        assert isinstance(notification, ServerNotification)
        self.server.notify_all_excluding(notification=notification, exclude=[self])

    # endregion
    ...


T = TypeVar('T', bound=RemoteSession)
SessionFactory = Callable[[SessionAdapter], T]


class Server(Generic[T], NetBase):
    """
    Network Server
    """

    def __init__(
            self,
            adapter: ServerAdapter,
            session_class: Optional[Type[T]] = None,
            session_factory: Optional[SessionFactory] = None,
            config: Optional[Dict[str, Any]] = None
    ) -> None:
        """
        See NetBase for more explanations on base class arguments

        Either pass a `session_class` or a `session_factory` but not both

        :param session_class: Type[T] - Class to be instanced when a new client connects
        :param session_factory: SessionFactory - Session factory to call when a new client connects
        """
        assert isinstance(adapter, ServerAdapter)

        super().__init__(adapter=adapter, config=config)

        self._session_class = session_class
        self._session_factory = session_factory if session_factory else self.default_session_factory
        self._sessions = {}  # type: Dict[int, T]

        self._default_action_context = ActionHistory[T]()
        self._action_contexts = {}  # type: Dict[uuid.UUID, ActionHistory[T]]

    # region Properties

    @property
    def adapter(self) -> ServerAdapter:
        """
        "Casted" adapter instance
        :return: ServerAdapter
        """
        assert isinstance(self._adapter, ServerAdapter)
        return self._adapter

    @property
    def sessions(self) -> ValuesView[T]:
        return self._sessions.values()

    @property
    def action_contexts(self) -> Dict[uuid.UUID, ActionHistory[T]]:
        return self._action_contexts

    # endregion

    def default_session_factory(self, session_adapter: SessionAdapter) -> T:
        """
        Default session factory.
        Returns an instance of the `session_class` passed to the constructor.

        :param session_adapter: SessionAdapter - Adapter data for the session
        :return:
        """
        assert isinstance(session_adapter, SessionAdapter)

        if self._session_class:
            return self._session_class(
                id=session_adapter.id,
                server=self
            )
        else:
            raise Exception("A session class is required in order to use the default session factory.")

    # region Lifecycle

    def start(self, port: int) -> None:
        """
        Start the server
        This method does a bit of preparation and then calls `_run` to really start the process.

        :param port: int - Port on which to listen for incoming connections
        :return: None
        """
        assert isinstance(port, int)

        logger.info("Starting server on port {}...".format(port))

        self.adapter.port = port
        self._run()

    def stop(self) -> None:
        """
        Stop the server
        This is only a semantic wrapper for the `_close` method.

        :return: None
        """

        logger.info("Stopping server...")

        self._close()

    def step(self, now: float, dt: float) -> None:
        super().step(now, dt)

        # Let each session have its own chance to a loop
        # Messages will already have been handled by super()
        for session in self._sessions.values():
            session.step(now, dt)

    # endregion

    # region Methods

    def disconnect(self, session: T) -> None:
        """
        Disconnect the session

        :param session: T
        :return: None
        """
        self._adapter.send_internal((InternalMessageId.SESSION_DISCONNECT, session.id))

    def register_action_context(self, context_id: uuid.UUID) -> ActionHistory[T]:
        """
        Register an action context.
        By default, context id 0 is used for the default action context (Undo Manager). Register new contexts when in
        need of managing multiple action histories for various clients or objects. When using multiple action contexts,
        a context myst be specified when creating an action or a transaction.

        :param context_id: uuid.UUID
        :return: ActionHistory[T, RemoteClient[T]]
        """

        if context_id in self._action_contexts:
            raise Exception("Context already registered")

        context = ActionHistory[T]()
        self._action_contexts[context_id] = context
        return context

    def unregister_action_context(self, context_id: UUID) -> None:
        """
        Unregister an action context.

        :param context_id: uuid.UUID
        :return: None
        """

        if context_id not in self._action_contexts:
            raise Exception("Context not registered")

        del self._action_contexts[context_id]

    def get_action_context(self, context_id: Optional[UUID] = None) -> Optional[ActionHistory[T]]:
        """
        Retrieve an action context
        By default, context id 0 is used for the default action context (Undo Manager). Register new contexts when in
        need of managing multiple action histories for various clients or objects. When using multiple action contexts,
        a context myst be specified when creating an action or a transaction.

        :param context_id: uuid.UUID
        :return: ActionHistory[T, RemoteClient[T]]
        """
        return self._action_contexts.get(context_id) if context_id else self._default_action_context

    def request_all(self, make_request: Callable[[T], ServerRequest]) -> None:
        """
        Send a request to every sessions.

        :param make_request: Callable[[T], ServerRequest] - Lambda/Callable creating the request for each session
        :return: None
        """

        # TODO: Maybe create a request type for when all sessions are concerned, we would save on serialization time

        for session in self.sessions:
            session.request(make_request(session))

    def command_all(self, command: Command) -> None:
        """
        Send a command to all sessions

        :param command: ServerCommand
        :return: None
        """

        # TODO: Maybe create a command type for when all sessions are concerned, we would save on serialization time

        for session in self.sessions:
            session.command(command)

    def notify_all(self, notification: ServerNotification) -> None:
        """
        Send a notification to subscribed sessions

        :param notification: ServerNotification
        :return: None
        """
        for session in self.sessions:
            session.notify(notification)

    def notify_all_excluding(self, notification: ServerNotification, exclude: Optional[List[RemoteSession]] = None) -> None:
        """
        Send a notification to subscribed sessions while excluding th sessions in exclude argument.

        :param notification: ServerNotification
        :param exclude: Optional[List[RemoteSession]]
        :return: None
        """
        if not exclude:
            self.notify_all(notification=notification)
        else:
            for session in self.sessions:
                if session in exclude:
                    continue
                session.notify(notification)

    # endregion

    # region Handlers

    def _on_message(self, message: Message) -> None:
        """
        Handle messages coming from client sessions
        If the message is a built-in type (i.e.: request, response, etc.) it will be handled here,
        otherwise, the message is passed to the to the remote session instance.

        :param message: Message
        :return: None
        """
        assert isinstance(message, Message)

        # TODO: This method fails "silently" and is hard to test, find a mechanism to test why it returned

        if message.session_id is None:
            logger.warning("Message has no session id")
            return

        if message.data is None:
            logger.warning("Message has no data")
            return

        session = self._sessions.get(message.session_id)
        if session:

            # NOTE: Checking by prefix is WAAYYY faster than with `isinstance`, we'll only assert the type for development
            message_prefix = message.data.serialization_prefix

            # if isinstance(message.data, Command) and not isinstance(message.data, ServerCommand):
            if message_prefix == SerializablePrefix.COMMAND or message_prefix == SerializablePrefix.BUILTIN_COMMAND:
                assert isinstance(message.data, Command) and not isinstance(message.data, ServerCommand)
                # Handle commands
                command = message.data
                command.handle(session=session)

            # elif isinstance(message.data, ClientRequest):
            elif message_prefix == SerializablePrefix.REQUEST:
                assert isinstance(message.data, ClientRequest)
                # Handle requests
                incoming_request = message.data
                request_session = session  # Just so that Mypy doesn't complain about it being optional in the callback

                def on_response(response: Response) -> None:
                    assert isinstance(response, ServerResponse)
                    response.request_id = incoming_request.id
                    # logger.debug("Sending response: {}".format(response))
                    response.on_sending(self)
                    request_session.send(Message(data=response))

                incoming_request.handle(session=session, respond=on_response)

            # elif isinstance(message.data, ClientResponse):
            elif message_prefix == SerializablePrefix.RESPONSE:
                assert isinstance(message.data, ClientResponse)
                # Handle responses
                incoming_response = message.data
                if incoming_response.request_id is not None:
                    try:
                        prior_request = self.requests.pop(incoming_response.request_id)
                    except KeyError:
                        logger.warning("Received a response without a prior request")
                    else:
                        incoming_response.handle(request=prior_request, session=session)
                else:
                    logger.warning("Received a response without a request id")

            # elif isinstance(message.data, Action):
            elif message_prefix == SerializablePrefix.ACTION:
                assert isinstance(message.data, Action)
                # Handle actions
                action = message.data
                context = self.get_action_context(action.context)
                if context:
                    context.apply(action=action, session=session)
                else:
                    logger.warning("Received action for invalid context ({})".format(action.context))

            # elif isinstance(message.data, ClientNotification):
            elif message_prefix == SerializablePrefix.NOTIFICATION:
                assert isinstance(message.data, ClientNotification)
                # Handle notifications
                notification = message.data
                notification.handle(session=session)

            else:
                # The rest goes to the handler for custom messages
                message.session = session
                session.on_message(message)

        else:
            # If we don't have a session with this id, it is not here that it should be created
            logger.warning("Received message for unknown session id {}".format(message.session_id))

    def _on_internal_message(self, message_id: InternalMessageId, payload: Any) -> None:
        """
        Handle internal messages from the network adapter

        :param message_id: InternalMessageId
        :param payload: Any
        :return: None
        """
        if message_id is InternalMessageId.SESSION_CONNECTED:
            assert isinstance(payload, SessionAdapter)
            connected_session_adapter = payload
            logger.info("Session {} connected".format(connected_session_adapter.id))
            connected_session = self._session_factory(connected_session_adapter)
            self._sessions[connected_session.id] = connected_session
            connected_session.on_connected()

        elif message_id is InternalMessageId.SESSION_DISCONNECTED:
            assert isinstance(payload, SessionAdapter)
            disconnected_session_adapter = payload
            logger.info("Session id: {} disconnected".format(disconnected_session_adapter.id))
            disconnected_session = self._sessions.get(disconnected_session_adapter.id)
            if not disconnected_session:
                logger.warning("Disconnected session does not exist")
                return
            del self._sessions[disconnected_session_adapter.id]
            disconnected_session.on_disconnected()

    # endregion
    ...
