"""
SAT's Python Networking Library
"""

__author__ = "François Ubald Brien, Emmanuel Durand"
__copyright__ = "Copyright 2017, Société des arts technologiques"
__credits__ = ["Nicolas Bouillot","François Ubald Brien", "Emmanuel Durand", "Michał Seta", "Jérémie Soria"]
__license__ = "GPLv3"
__version__ = "1.0.0"
__maintainer__ = "Emmanuel Durand"
__email__ = "metalab@sat.qc.ca"
__status__ = "Development"


from enum import IntEnum, unique

# Set to True if you want to enable hex dumps of messages (impacts performance)
# and also requires the logger 'satnet.adapters' to be set to DEBUG.
HEX_DUMP = False


@unique
class SerializablePrefix(IntEnum):
    """
    Serialization prefixes for various built-in types
    """
    BUILTIN_COMMAND = 0x01

    COMMAND = 0x02
    REQUEST = 0x03
    RESPONSE = 0x04
    ACTION = 0x05
    NOTIFICATION = 0x06

    ENTITY = 0x07
    CUSTOM = 0x08
