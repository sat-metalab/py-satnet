@startuml

class Entity {
    #_uuid:UUID
    #_ready:bool
    +ready()
}
Entity -up-|> Serializable

' MESSAGE

enum MessageDataType {
    RAW
    SERIALIZED
}

enum InternalMessageId {
    CONNECTED
    DISCONNECTED
    DISCONNECT
    SESSION_CONNECTED
    SESSION_DISCONNECTED
    SESSION_DISCONNECT
}

class Message {
    data:Optional[Serializable]
    raw:Optional[bytes]
    session:Optional[Session]
    session_id:Optional[int]
}
Message "1" o-- "0..1" Serializable
Message "1" o-left- "0..1" Session

class MessageParser {
    +from_bytes(data:bytes, offset:int):Message
    +to_bytes(message:Message):bytes
}

' ACTION

class Action<T=RemoteSession> {
    #_context:UUID
    +apply(session:T)
    +revert(session:T)
}
Action -up-|> Serializable

class Transaction<T=RemoteSession> {
    #_actions::List[Action[T]]
    +apply(session:T)
    +revert(session:T)
}
Transaction -up-|> Action

class ActionHistory<T=RemoteSession> {
    #_history:List[Action[T]]
    _pointer:int
    +apply(action:Action[T], session:T)
    +undo(session:T)
    +redo(session:T)
    +reset()
}

' COMMAND

class Command<S=RemoteSession> {
    +handle(session:S)
}
Command -up-|> Serializable

class ClientCommand<RS=RemoteSession>
ClientCommand -up-|> Command : S=RS

class ServerCommand<LS=LocalSession>
ServerCommand -up-|> Command : S=LS

' NOTIFICATION

class Notification<S=Session> {
    #_context:UUID
    +topic:int
    +handle(session:S)
}
Notification -up-|> Serializable

class ClientNotification<RS=RemoteSession>
ClientNotification -up-|> Notification : S=RS

class ServerNotification<LS=LocalSession>
ServerNotification -up-|> Notification : S=LS

' REQUEST & RESPONSE

class Request<S=Session> {
    #_id:int
    +handle(session:S, respond:Callable[['Response'], None])
}
Request -up-|> Serializable

class ClientRequest<RS=RemoteSession>
ClientRequest -up-|> Request : S=RS

class ServerRequest<LS=LocalSession>
ServerRequest -up-|> Request : S=LS

class Response<S=Session\nR=Request> {
    #request_id:int
    +handle(request:R, session:S)
}
Response -up-|> Serializable

class ClientResponse<RS=RemoteSession\nSR=ServerRequest>
ClientResponse -up-|> Response : S=RS\nR=SR

class ServerResponse<LS=LocalSession\nCR=ClientRequest>
ServerResponse -up-|> Response : S=LS\nR=CR

@enduml
