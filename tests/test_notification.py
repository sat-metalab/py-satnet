from unittest import TestCase, mock
from satnet.serialization import Serializable
from .mock.notification import DummySession, MockNotification


class TestNotification(TestCase):

    def setUp(self):
        self.session = DummySession()

    def test_notification(self):
        notification = MockNotification(context=0xFF)
        self.assertAlmostEqual(notification.topic, 0xF0)
        self.assertAlmostEqual(notification.context, 0xFF)

    def test_serialization(self):
        notification1 = MockNotification(context=123)
        notification2 = Serializable.deserialize(notification1.serialize())
        self.assertIsNotNone(notification2)
        self.assertEqual(notification1.topic, notification2.topic)
        self.assertEqual(notification1.context, notification2.context)

