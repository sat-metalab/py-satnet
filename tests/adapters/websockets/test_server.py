from unittest import mock

import aiounittest
import asynctest
import websockets
from asynctest import CoroutineMock

from satnet.adapters.websockets.server import WSServer, WSConnection
from satnet.message import InternalMessageId
from .mock.websockets import MockWSConnection, MockWSServer


class TestWSConnection(aiounittest.AsyncTestCase):
    def setUp(self):
        self.addCleanup(mock.patch.stopall)

    def test_connection(self):
        server = WSServer()
        connection = MockWSConnection(server)
        self.assertIsInstance(connection.server, WSServer)
        self.assertIs(connection.server, server)


class TestWSServer(aiounittest.AsyncTestCase):
    def setUp(self):
        self.addCleanup(mock.patch.stopall)

    async def test_handler_ok(self):
        server = MockWSServer()
        server.consumer_handler = CoroutineMock(return_value=None)
        server.producer_handler = CoroutineMock(return_value=None)

        socket = mock.Mock()
        await server.handler(socket, '')
        self.assertEqual(len(server._session_adapters), 1)
        msg = server._internal_incoming.popleft()
        self.assertEqual(msg[0], InternalMessageId.SESSION_CONNECTED)
        self.assertIsInstance(msg[1], WSConnection)
        msg = server._internal_incoming.popleft()
        self.assertEqual(msg[0], InternalMessageId.SESSION_DISCONNECTED)
        self.assertIsInstance(msg[1], WSConnection)

    async def test_handler_lost_connection(self):
        server = MockWSServer()
        server.consumer_handler = CoroutineMock(return_value=None)
        server.producer_handler = CoroutineMock(return_value=None)

        socket = mock.Mock()
        with asynctest.mock.patch('asyncio.wait', side_effect=websockets.ConnectionClosed(code=1,
                                                                                          reason='Lost connection')):
            await server.handler(socket, '')
            # Session adapter is deleted when an exception occurs
            self.assertEqual(len(server._session_adapters), 0)
            msg = server._internal_incoming.popleft()
            self.assertEqual(msg[0], InternalMessageId.SESSION_CONNECTED)
            self.assertIsInstance(msg[1], WSConnection)
            msg = server._internal_incoming.popleft()
            self.assertEqual(msg[0], InternalMessageId.SESSION_DISCONNECTED)
            self.assertIsInstance(msg[1], WSConnection)





