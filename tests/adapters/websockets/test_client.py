from unittest import mock

import aiounittest
import asynctest
import websockets
from asynctest import CoroutineMock

from satnet.message import InternalMessageId
from .mock.websockets import MockWSClient, MagicMockContext


class TestWSClient(aiounittest.AsyncTestCase):
    def setUp(self):
        self.addCleanup(mock.patch.stopall)

    def test_run(self):
        client = MockWSClient()
        client.handler = CoroutineMock(return_value=None)

        with asynctest.mock.patch('websockets.connect', new_callable=MagicMockContext) as p:
            client.start()
            self.assertFalse(client._ev_loop.is_running())
            self.assertEqual(len(client._internal_incoming), 1)
            self.assertEqual(client._internal_incoming.popleft()[0], InternalMessageId.CONNECTED)
            # We just check that the exceptions are caught correctly
            p.side_effect = ConnectionResetError()
            client.start()
            p.side_effect = ConnectionRefusedError()
            client.start()
            client.stop()

    def test_handler_ok(self):
        client = MockWSClient()
        client.consumer_handler = CoroutineMock(return_value=None)
        client.producer_handler = CoroutineMock(return_value=None)

        with asynctest.mock.patch('websockets.connect', new_callable=MagicMockContext):
            client.start()
            self.assertEqual(client._internal_incoming.popleft()[0], InternalMessageId.CONNECTED)
            self.assertEqual(client._internal_incoming.popleft()[0], InternalMessageId.SESSION_CONNECTED)
            self.assertEqual(client._internal_incoming.popleft()[0], InternalMessageId.SESSION_DISCONNECTED)
            client.stop()

    def test_handler_lost_connection(self):
        client = MockWSClient()
        client.consumer_handler = CoroutineMock(return_value=None)
        client.producer_handler = CoroutineMock(return_value=None)

        with asynctest.mock.patch('asyncio.wait', side_effect=websockets.ConnectionClosed(code=1,
                                                                                          reason='Lost connection')):
            with asynctest.mock.patch('websockets.connect', new_callable=MagicMockContext):
                client.start()
                self.assertEqual(client._internal_incoming.popleft()[0], InternalMessageId.CONNECTED)
                self.assertEqual(client._internal_incoming.popleft()[0], InternalMessageId.SESSION_CONNECTED)
                self.assertEqual(client._internal_incoming.popleft()[0], InternalMessageId.SESSION_DISCONNECTED)
                client.stop()
