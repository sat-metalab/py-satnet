from unittest import TestCase, mock

import zmq
from zmq import Context, Again

from satnet.errors import ParsingError
from satnet.message import Message, InternalMessageId
from satnet.adapters.zmq.base import ZMQFrameType, ZMQBase
from .mock.zmq import MockZMQClient


class TestZMQClient(TestCase):

    def setUp(self):
        self.addCleanup(mock.patch.stopall)

    def test_initialization(self):
        client = MockZMQClient()
        self.assertFalse(client._connected)
        
    def test_close(self):
        client = MockZMQClient()
        client._connected = True
        client.stop()
        self.assertFalse(client._connected)

    def test_run(self):
        client = MockZMQClient()
        client._abort = True  # We don't want to enter the loop
        self.assertEqual(len(client._internal_incoming), 0)
        with mock.patch.object(ZMQBase, 'start') as r:
            client.start()
            r.assert_called_once_with()
        self.assertEqual(len(client._internal_incoming), 1)
        self.assertEqual(client._internal_incoming.popleft(), (InternalMessageId.DISCONNECTED, None))

    def test_get_socket(self):
        client = MockZMQClient()
        client._host = "host"
        client._port = 1234
        socket = mock.Mock()
        context = mock.Mock()
        context.socket.return_value = socket
        s = client._get_socket(context)
        self.assertEqual(s, socket)
        context.socket.assert_called_once_with(zmq.DEALER)
        socket.setsockopt.assert_called_with(zmq.RCVHWM, 0)
        socket.connect.assert_called_once_with("tcp://host:1234")

    def test_wrap(self):
        mp = mock.Mock()
        mp.to_bytes.return_value = b'test'
        client = MockZMQClient(message_parser=mp)
        time = mock.patch('time.time', return_value=1234).start()
        message = Message()
        frames = client._wrap(message)
        self.assertEqual(frames, (b'', bytes([ZMQFrameType.CUSTOM.value]) + b'test'))
        self.assertEqual(client._last_sent_heartbeat, 1234)
        mp.to_bytes.assert_called_once_with(message)

    # region unwrap

    def test_unwrap_heartbeat(self):
        client = MockZMQClient()
        time = mock.patch('time.time', return_value=1234).start()
        client._unwrap([b'', bytes([ZMQFrameType.HEARTBEAT.value])])
        self.assertEqual(client._last_received_heartbeat, 1234)

    def test_unwrap_heartbeat_any(self):
        message = Message()
        mp = mock.Mock()
        mp.from_bytes.return_value = message
        client = MockZMQClient(message_parser=mp)
        time = mock.patch('time.time', return_value=1234).start()
        client._unwrap([b'', bytes([ZMQFrameType.CUSTOM.value])])
        self.assertEqual(client._last_received_heartbeat, 1234)

    def test_unwrap_connection(self):
        message = Message()
        mp = mock.Mock()
        mp.from_bytes.return_value = message
        client = MockZMQClient(message_parser=mp)
        client._unwrap([b'', bytes([ZMQFrameType.CUSTOM.value])])
        self.assertTrue(client._connected)
        self.assertEqual(client._internal_incoming.popleft(), (InternalMessageId.CONNECTED, None))

    def test_unwrap_connected(self):
        message = Message()
        mp = mock.Mock()
        mp.from_bytes.return_value = message
        client = MockZMQClient(message_parser=mp)
        client._connected = True
        client._unwrap([b'', bytes([ZMQFrameType.CUSTOM.value])])
        self.assertTrue(client._connected)
        self.assertEqual(len(client._internal_incoming), 0)

    def test_unwrap_custom(self):
        message = Message()
        mp = mock.Mock()
        mp.from_bytes.return_value = message
        client = MockZMQClient(message_parser=mp)
        client._connected = True
        client._unwrap([b'', bytes([ZMQFrameType.CUSTOM.value])])
        mp.from_bytes.assert_called_once_with(bytes([ZMQFrameType.CUSTOM.value]), 1)
        self.assertEqual(len(client._network_incoming), 1)
        self.assertEqual(client._network_incoming.popleft(), message)

    def test_unwrap_custom_failed(self):
        mp = mock.Mock()
        mp.from_bytes.side_effect = ParsingError
        client = MockZMQClient(message_parser=mp)
        client._connected = True
        client._unwrap([b'', bytes([ZMQFrameType.CUSTOM.value])])
        mp.from_bytes.assert_called_once_with(bytes([ZMQFrameType.CUSTOM.value]), 1)
        self.assertEqual(len(client._network_incoming), 0)

    def test_unwrap_connection_closed(self):
        client = MockZMQClient()
        with mock.patch.object(client, 'stop') as stop:
            client._unwrap([b'', bytes([ZMQFrameType.CONNECTION_CLOSED.value])])
            stop.assert_called_once_with()

    # endregion

    # region step

    def test_step_init(self):
        client = MockZMQClient()
        client._run(now=1234, dt=0.5)
        self.assertEqual(client._last_received_heartbeat, 1234)

    def test_step_nothing_to_do(self):
        client = MockZMQClient()
        client._socket = mock.Mock()

        client.heartbeat_interval = 1.00
        client.connection_timeout = 3.00
        client._last_received_heartbeat = 1233.5
        client._last_sent_heartbeat = 1233.5

        with mock.patch.object(client, 'stop') as stop:
            client._run(now=1234, dt=0.5)
            stop.assert_not_called()
            client._socket.send_multipart.assert_not_called()

    def test_step_timeout(self):
        client = MockZMQClient()
        client._socket = mock.Mock()

        client.heartbeat_interval = 1.00
        client.connection_timeout = 3.00
        client._last_received_heartbeat = 1231
        client._last_sent_heartbeat = 1233.5

        with mock.patch.object(client, 'stop') as stop:
            client._run(now=1234, dt=0.5)
            stop.assert_called_once_with()
            client._socket.send_multipart.assert_not_called()

    def test_step_heartbeat(self):
        client = MockZMQClient()
        client._socket = mock.Mock()

        client.heartbeat_interval = 1.00
        client.connection_timeout = 3.00
        client._last_received_heartbeat = 1233.5
        client._last_sent_heartbeat = 1233

        with mock.patch.object(client, 'stop') as stop:
            client._run(now=1234, dt=0.5)
            stop.assert_not_called()
            client._socket.send_multipart.assert_called_once_with((b'', bytes([ZMQFrameType.HEARTBEAT.value])),zmq.DONTWAIT)
            self.assertEqual(client._last_sent_heartbeat, 1234)

    def test_step_heartbeat_full_queue(self):
        client = MockZMQClient()
        client._socket = mock.Mock()
        client._socket.send_multipart.side_effect = Again

        client.heartbeat_interval = 1.00
        client.connection_timeout = 3.00
        client._last_received_heartbeat = 1233.5
        client._last_sent_heartbeat = 1233

        with mock.patch.object(client, 'stop') as stop:
            client._run(now=1234, dt=0.5)
            client._socket.send_multipart.assert_called_once_with((b'', bytes([ZMQFrameType.HEARTBEAT.value])), zmq.DONTWAIT)
            stop.assert_called_once_with()
            self.assertEqual(client._last_sent_heartbeat, 1234)

    # endregion
