from satnet.adapters.zmq.base import ZMQBase
from satnet.adapters.zmq.client import ZMQClient
from satnet.adapters.zmq.server import ZMQServer, ZMQSession


class MockBase(ZMQBase):

    def _get_socket(self, context):
        pass

    def _on_internal_message(self, message_id, payload):
        pass

    def _run(self):
        pass

    def _unwrap(self, msg):
        pass

    def _wrap(self, message):
        pass


class MockZMQClient(ZMQClient):
    pass


class MockZMQServer(ZMQServer):
    pass


class MockZMQSession(ZMQSession):
    pass
