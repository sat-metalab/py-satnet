from unittest import TestCase, mock

from satnet.message import Message
from .mock.base import MockSession, MockBase, MockBaseAdapter
from .mock.client import MockClientAdapter
from .mock.request import MockRequest


class TestClient(TestCase):
    def setUp(self):
        self.adapter = MockBaseAdapter()
        # self.net = MockClientAPI()
        self.session = MockSession()
        self.adapter = MockClientAdapter()

    def create_base(self):
        return MockBase(adapter=self.adapter)

    def test_close(self):
        base = self.create_base()
        with mock.patch.object(base, '_close_connection') as cc:
            base._close()
            cc.assert_called_once_with()

    # _close_connection is weird to test, skipping

    def test_step_nothing(self):
        base = self.create_base()
        with mock.patch.object(base, '_on_internal_message') as oim, \
                mock.patch.object(base, '_on_message') as om:
            base.step(123, 456)
            oim.assert_not_called()
            om.assert_not_called()

    def test_step_internal_message(self):
        base = self.create_base()
        self.adapter._internal_incoming.append((0xFF, "Something"))
        with mock.patch.object(base, '_on_internal_message') as oim, \
                mock.patch.object(base, '_on_message') as om:
            base.step(123, 456)
            oim.assert_called_once_with(0xFF, "Something")
            om.assert_not_called()

    def test_step_message(self):
        base = self.create_base()
        message = Message()
        self.adapter._network_incoming.append(message)
        with mock.patch.object(base, '_on_internal_message') as oim, \
                mock.patch.object(base, '_on_message') as om:
            base.step(123, 456)
            oim.assert_not_called()
            om.assert_called_once_with(message)

    # region Middleware

    # def test_step_message_middleware_continue(self):
    #     base = self.create_base()
    #     middleware = MockMiddleware()
    #     base.add_middleware(middleware)
    #     message = Message()
    #     self.adapter._network_incoming.append(message)
    #     with mock.patch.object(middleware, 'handle', return_value=True) as mwh, \
    #             mock.patch.object(base, '_on_internal_message') as oim, \
    #             mock.patch.object(base, '_on_message') as om:
    #         base.step(123, 456)
    #         oim.assert_not_called()
    #         mwh.assert_called_once_with(self.session, base, message)
    #         om.assert_called_once_with(message)
    # 
    # def test_step_message_middleware_break(self):
    #     base = self.create_base()
    #     middleware = MockMiddleware()
    #     base.add_middleware(middleware)
    #     message = Message()
    #     self.adapter._network_incoming.append(message)
    #     with mock.patch.object(middleware, 'handle', return_value=False) as mwh, \
    #             mock.patch.object(base, '_on_internal_message') as oim, \
    #             mock.patch.object(base, '_on_message') as om:
    #         base.step(123, 456)
    #         oim.assert_not_called()
    #         mwh.assert_called_once_with(self.session, base, message)
    #         om.assert_not_called()
    # 
    # def test_add_middleware(self):
    #     base = self.create_base()
    #     middleware = MockMiddleware()
    #     base.add_middleware(middleware)
    #     self.assertEqual(len(base._middleware), 1)
    #     self.assertEqual(base._middleware[0], middleware)
    # 
    # def test_add_middleware_not_middleware(self):
    #     base = self.create_base()
    #     with self.assertRaises(AssertionError):
    #         base.add_middleware("Not a middleware")
    #         self.assertEqual(len(base._middleware), 0)

    # endregion

    # region Send

    def test_send(self):
        base = self.create_base()
        message = Message()
        with mock.patch.object(self.adapter, 'send') as send:
            base.send(message)
            send.assert_called_once_with(message)

    def test_send_not_message(self):
        base = self.create_base()
        with self.assertRaises(AssertionError), mock.patch.object(self.adapter, 'send') as send:
            base.send("Not a message")
            send.assert_not_called()

    # endregion

    # region Request

    def test_get_request(self):
        base = self.create_base()
        request = MockRequest()
        message = base.get_request(request=request)
        self.assertEqual(base._requests.get(request.id), request)
        self.assertIsNotNone(message)
        self.assertIsInstance(message, Message)
        self.assertEqual(message.data, request)

    def test_get_request_not_request(self):
        base = self.create_base()
        with self.assertRaises(AssertionError):
            message = base.get_request(request="Not a request")
            self.assertIsNone(message)
        self.assertEqual(len(base._requests.items()), 0)

    # endregion
