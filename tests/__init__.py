import os
import sys

# Add paths to local libs for tests
path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/satlib")))
