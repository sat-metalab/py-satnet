from unittest import TestCase, mock
from satnet.serialization import Serializable
from satnet.action import ActionHistory
from satnet.action import Transaction
from .mock.actions import MockAction, DummySession


class TestAction(TestCase):

    def setUp(self):
        self.session = DummySession()

    def test_transaction_apply(self):
        dc1 = MockAction()
        dc2 = MockAction()

        t = Transaction([dc1, dc2])

        with mock.patch.object(dc1, 'apply', wraps=dc1.apply) as dc1_apply, \
             mock.patch.object(dc2, 'apply', wraps=dc2.apply) as dc2_apply:
            t.apply(session=self.session)
            dc1_apply.assert_called_once_with(session=self.session)
            dc2_apply.assert_called_once_with(session=self.session)

    def test_transaction_revert(self):
        dc1 = MockAction()
        dc2 = MockAction()

        t = Transaction([dc1, dc2])
        t.apply(session=self.session)

        with mock.patch.object(dc1, 'revert', wraps=dc1.revert) as dc1_revert, \
             mock.patch.object(dc2, 'revert', wraps=dc2.revert) as dc2_revert:
            t.revert(session=self.session)
            # Unfortunately we can't test for call order...
            dc1_revert.assert_called_once_with(session=self.session)
            dc2_revert.assert_called_once_with(session=self.session)

    def test_action_serialization(self):
        action1 = MockAction(context=123)
        action2 = Serializable.deserialize(action1.serialize())
        self.assertIsNotNone(action2)
        self.assertEqual(action1.context, action2.context)

    def test_transaction_serialization(self):
        action1 = MockAction(context=123)
        action2 = MockAction(context=456)
        action3 = MockAction(context=789)
        transaction1 = Transaction(actions=[action1, action2, action3])
        transaction2 = Serializable.deserialize(transaction1.serialize())
        self.assertIsNotNone(transaction2)
        self.assertEqual(len(transaction1._actions), len(transaction2._actions))


class TestActionHistory(TestCase):

    def setUp(self):
        self.session = DummySession()
        self.history = ActionHistory()

    def test_reset(self):
        cmd = MockAction()
        self.history.apply(cmd, session=self.session)
        self.history.reset()
        self.assertEqual(self.history._pointer, 0)
        self.assertEqual(len(self.history._history), 0)

    def test_apply(self):
        cmd = MockAction()
        with mock.patch.object(cmd, 'apply', wraps=cmd.apply) as apply:
            self.history.apply(cmd, session=self.session)
            apply.assert_called_once_with(session=self.session)
        self.assertEqual(self.history._pointer, 1)
        self.assertEqual(len(self.history._history), 1)
        self.assertEqual(self.history._history[0], cmd)

    def test_apply_multi(self):
        cmd1 = MockAction()
        cmd2 = MockAction()
        cmd3 = MockAction()
        self.history.apply(cmd1, session=self.session)
        self.history.apply(cmd2, session=self.session)
        self.history.apply(cmd3, session=self.session)
        self.assertEqual(self.history._pointer, 3)
        self.assertEqual(len(self.history._history), 3)
        self.assertEqual(self.history._history[0], cmd1)
        self.assertEqual(self.history._history[1], cmd2)
        self.assertEqual(self.history._history[2], cmd3)

    def test_undo(self):
        cmd = MockAction()
        self.history.apply(cmd, session=self.session)
        with mock.patch.object(cmd, 'revert', wraps=cmd.revert) as revert:
            self.history.undo(session=self.session)
            revert.assert_called_once_with(session=self.session)
        # Only the pointer should have changed
        self.assertEqual(self.history._pointer, 0)
        self.assertEqual(len(self.history._history), 1)
        self.assertEqual(self.history._history[0], cmd)

    def test_undo_multi(self):
        cmd1 = MockAction()
        cmd2 = MockAction()
        cmd3 = MockAction()
        self.history.apply(cmd1, session=self.session)
        self.history.apply(cmd2, session=self.session)
        self.history.apply(cmd3, session=self.session)
        with mock.patch.object(cmd3, 'revert', wraps=cmd3.revert) as revert:
            self.history.undo(session=self.session)
            revert.assert_called_once_with(session=self.session)
        # Only the pointer should have changed
        self.assertEqual(self.history._pointer, 2)
        self.assertEqual(len(self.history._history), 3)
        self.assertEqual(self.history._history[0], cmd1)
        self.assertEqual(self.history._history[1], cmd2)
        self.assertEqual(self.history._history[2], cmd3)

    def test_undo_overwrite(self):
        cmd1 = MockAction()
        cmd2 = MockAction()
        self.history.apply(cmd1, session=self.session)
        self.history.undo(session=self.session)
        self.history.apply(cmd2, session=self.session)
        self.assertEqual(self.history._pointer, 1)
        self.assertEqual(len(self.history._history), 1)
        self.assertEqual(self.history._history[0], cmd2)

    def test_undo_overwrite_multi(self):
        cmd1 = MockAction()
        cmd2 = MockAction()
        cmd3 = MockAction()
        cmd4 = MockAction()
        self.history.apply(cmd1, session=self.session)
        self.history.apply(cmd2, session=self.session)
        self.history.apply(cmd3, session=self.session)
        self.history.undo(session=self.session)
        self.history.apply(cmd4, session=self.session)
        self.assertEqual(self.history._pointer, 3)
        self.assertEqual(len(self.history._history), 3)
        self.assertEqual(self.history._history[0], cmd1)
        self.assertEqual(self.history._history[1], cmd2)
        self.assertEqual(self.history._history[2], cmd4)

    def test_redo(self):
        cmd = MockAction()
        self.history.apply(cmd, session=self.session)
        self.history.undo(session=self.session)
        with mock.patch.object(cmd, 'apply', wraps=cmd.apply) as apply:
            self.history.redo(session=self.session)
            apply.assert_called_once_with(session=self.session)
        self.assertEqual(self.history._pointer, 1)
        self.assertEqual(len(self.history._history), 1)
        self.assertEqual(self.history._history[0], cmd)
