from unittest import TestCase, mock

from satnet.serialization import Serializable
from .mock.entity import MockEntity


class TestEntity(TestCase):

    def test_uuid_serialization(self):
        entity = MockEntity()
        entity_prime = Serializable.deserialize(entity.serialize())
        self.assertEqual(entity.uuid, entity_prime.uuid)
