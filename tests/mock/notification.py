from satnet.notification import Notification, notification


class DummySession:
    pass


@notification(id=0xF0)
class MockNotification(Notification[DummySession]):

    def handle(self, session: DummySession):
        pass