from typing import Any

from satnet.session import Session
from satnet.base import NetBase, BaseAdapter
from satnet.message import Message, InternalMessageId
from satnet.command import Command
from satnet.message import Message
from satnet.notification import Notification
from satnet.request import Request


class MockSession(Session):

    def __init__(self, net=None):
        super().__init__(net=net)

    def command(self, command: Command):
        pass

    def disconnect(self):
        pass

    def notify(self, notification: Notification):
        pass

    def request(self, request: Request):
        pass

    def send(self, message: Message):
        pass


class MockBase(NetBase):

    def _on_message(self, message: Message):
        pass

    def _on_internal_message(self, message_id: InternalMessageId, payload: Any):
        pass


class MockBaseAdapter(BaseAdapter):

    def _on_internal_message(self, message_id: InternalMessageId, payload: Any):
        pass

    def stop(self):
        pass

    def start(self):
        pass

    def run(self, now: float, dt: float):
        pass

# class MockMiddleware(Middleware[DummyTarget]):
#
#     def handle(self, target: DummyTarget, net: 'NetBase', message: 'Message'):
#         pass
