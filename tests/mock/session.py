from satnet.session import Session


class MockSession(Session):
    def step(self, now: float, dt: float):
        pass

    def disconnect(self):
        pass
