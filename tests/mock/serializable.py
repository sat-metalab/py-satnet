from typing import Callable

from satnet.serialization import Serializable, serializable


class MockSerializableA(Serializable):
    _fields = ['a', 'z']


class MockSerializableB(MockSerializableA):
    _fields = ['a', 'b']


class MockSerializableC(MockSerializableB):
    _fields = ['b', 'c', 'a', 'y']


class MockSerializableBNoFields(MockSerializableA):
    pass


class MockSerializableCNoFields(MockSerializableBNoFields):
    _fields = ['m', 'n']


@serializable(prefix=0xFF, id=0xE4)
class DummySerializable(Serializable):

    name = "Auto Name"

    _fields = [
        'name'
    ]


@serializable(prefix=0xFF, id=0xE3)
class DummyExtSerializable(Serializable):
    list = []
    _fields = ['list']


@serializable(prefix=0xFF, id=0xE1)
class DummyManualSerializable(Serializable):

    name = "Manual Name"
    target = 1
    value = 0.1

    def pack(self, write: Callable) -> None:
        super().pack(write)
        write(self.name)
        write(self.target)
        write(self.value)

    def unpack(self, read: Callable) -> None:
        super().unpack(read)
        self.name = read()
        self.target = read()
        self.value = read()


@serializable(prefix=0xFF, id=0xE2)
class DummyAutoSerializable(Serializable):

    name = "Auto Name"
    target = 9000
    value = 0.9

    _fields = [
        'name',
        'target',
        'value'
    ]


@serializable(prefix=0xFF, id=0xE0)
class DummyPrefixedSerializable(Serializable):

    name = "Prefix Name"
    target = 9999
    value = 0.999

    _fields = [
        'name',
        'target',
        'value'
    ]


@serializable(prefix=0xFF, id=0xE5)
class DummyContainerSerializable(Serializable):
    _fields = ['children']
    children = []
