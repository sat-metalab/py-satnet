from satnet.command import Command, command, ClientCommand, ServerCommand
from .base import MockSession


@command(id=0xF0)
class MockCommand(Command[MockSession]):
    def handle(self, session: MockSession):
        pass


@command(id=0xF1)
class MockClientCommand(ClientCommand[MockSession]):
    def handle(self, session: MockSession):
        pass


@command(id=0xF2)
class MockServerCommand(ServerCommand[MockSession]):
    def handle(self, session: MockSession):
        pass
