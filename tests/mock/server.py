from typing import Any
from satnet.server import Server, ServerAdapter, RemoteSession, SessionAdapter
from satnet.message import InternalMessageId


class MockServer(Server):
    pass


class MockServerAdapter(ServerAdapter):

    def stop(self):
        pass

    def start(self):
        pass

    def run(self, now: float, dt: float):
        pass

    def _on_internal_message(self, message_id: InternalMessageId, payload: Any):
        pass


class MockRemoteSession(RemoteSession):

    def step(self, now: float, dt: float):
        pass


class MockRemoteClientAdapter(SessionAdapter):

    def step(self, now: float, dt: float):
        pass
