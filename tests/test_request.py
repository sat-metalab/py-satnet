from unittest import TestCase
from satnet.serialization import Serializable
from satnet.request import Request
from .mock.request import MockRequest, MockResponse


class TestResponse(TestCase):

    def test_serialization(self):
        response = MockResponse()
        response.request_id = 123  # Force the id to something we'll recognize
        res = Serializable.deserialize(response.serialize())
        self.assertIsNotNone(res)
        self.assertEqual(res.request_id, response.request_id)


class TestRequest(TestCase):
    def setUp(self):
        # Should we set the parent (Request) _id_counter variable?
        # Or should MockRequest have its own _id and _id_counter system?
        Request._id_counter = -1

    def test_id_generation(self):
        self.assertEqual(MockRequest._id_counter, -1)
        id = MockRequest.get_id()
        self.assertEqual(MockRequest._id_counter, 0)
        self.assertEqual(id, 0)

    def test_id_assignation(self):
        request = MockRequest()
        self.assertEqual(request.id, 0)

    def test_serialization(self):
        request = MockRequest()
        request._id = 123  # Force the id to something we'll recognize
        req = Serializable.deserialize(request.serialize())
        self.assertIsNotNone(req)
        self.assertEqual(req.id, request.id)
