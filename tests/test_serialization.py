import unittest
from unittest import TestCase, mock

from satnet.serialization import Serializable
from .mock.serializable import MockSerializableBNoFields, MockSerializableCNoFields, DummyAutoSerializable, DummyContainerSerializable, DummyExtSerializable, DummyManualSerializable, DummySerializable, MockSerializableC


class TestSerialization(TestCase):

    def test_initializer_construction(self):
        with mock.patch.object(DummySerializable, 'initialize') as initialize:
            DummySerializable()
            initialize.assert_called_once_with()

    def test_initializer_deserialization(self):
        s = DummySerializable().serialize()
        with mock.patch.object(DummySerializable, 'initialize') as initialize:
            Serializable.deserialize(s)
            initialize.assert_called_once_with()

    def test_combines_fields(self):
        s = MockSerializableC()
        self.assertEqual(['a', 'z', 'b', 'c', 'y'], s.fields)

    def test_combine_fields_when_no_fields_member_in_hierarchy_1(self):
        """ Just a safety, in case some intermediate class doesn't declare any fields """
        s = MockSerializableCNoFields()
        self.assertEqual(['a', 'z', 'm', 'n'], s.fields)

    def test_combine_fields_when_no_fields_member_in_hierarchy_2(self):
        """ Just a safety, in case some intermediate class doesn't declare any fields """
        s = MockSerializableBNoFields()
        self.assertEqual(['a', 'z'], s.fields)

    @unittest.skip
    def test_equality1(self):
        """ Equality operator was removed as it was not true equality """
        dc1 = DummyManualSerializable()
        dc1.name = "Dummy Serializable"
        dc1.target = 123
        dc1.value = 0.976
        dc2 = DummyManualSerializable()
        dc2.name = "Dummy Serializable"
        dc2.target = 123
        dc2.value = 0.976
        self.assertEqual(dc1, dc2)

    @unittest.skip
    def test_equality2(self):
        """ Equality operator was removed as it was not true equality """
        dc1 = DummyManualSerializable()
        dc1.name = "Dummy Serializable"
        dc1.target = 123
        dc1.value = 0.976
        dc2 = DummyManualSerializable()
        dc2.name = "Dummy Serializable Not"
        dc2.target = 123
        dc2.value = 0.976
        self.assertEqual(dc1, dc2)

    def test_manual_roundtrip(self):
        dc = DummyManualSerializable()
        dc.name = "Dummy Serializable"
        dc.target = 123
        dc.value = 0.976
        ndc = Serializable.deserialize(dc.serialize())
        self.assertIsNotNone(ndc)
        self.assertTrue(isinstance(ndc, DummyManualSerializable))
        self.assertEqual(dc.name, ndc.name)
        self.assertEqual(dc.target, ndc.target)
        self.assertEqual(dc.value, ndc.value)

    def test_auto_roundtrip(self):
        dc = DummyAutoSerializable()
        dc.name = "Dummy Serializable"
        dc.target = 123
        dc.value = 0.976
        ndc = Serializable.deserialize(dc.serialize())
        self.assertIsNotNone(ndc)
        self.assertTrue(isinstance(ndc, DummyAutoSerializable))
        self.assertEqual(dc.name, ndc.name)
        self.assertEqual(dc.target, ndc.target)
        self.assertEqual(dc.value, ndc.value)

    # They are all prefixed for now
    # def test_prefixed(self):
    #     dc = DummyPrefixedSerializable()
    #     dc.name = "Dummy Serializable"
    #     dc.target = 123
    #     dc.value = 0.976
    #     ndc = Serializable.deserialize(dc.serialize())
    #     self.assertIsNotNone(ndc)
    #     self.assertTrue(isinstance(ndc, DummyPrefixedSerializable))
    #     self.assertEqual(dc.name, ndc.name)
    #     self.assertEqual(dc.target, ndc.target)
    #     self.assertEqual(dc.value, ndc.value)

    def test_ext(self):
        dc1 = DummyManualSerializable()
        dc1.name = "Dummy Serializable"
        dc1.target = 123
        dc1.value = 0.976

        dc2 = DummyAutoSerializable()
        dc2.name = "Dummy Serializable"
        dc2.target = 456
        dc2.value = 0.123

        t = DummyExtSerializable()
        t.list = [dc1, dc2]
        nt = Serializable.deserialize(t.serialize())
        self.assertIsNotNone(nt)
        self.assertTrue(isinstance(nt, DummyExtSerializable))
        self.assertEqual(len(nt.list), len(t.list))

        self.assertTrue(isinstance(nt.list[0], DummyManualSerializable))
        self.assertEqual(nt.list[0].name, t.list[0].name)
        self.assertEqual(nt.list[0].target, t.list[0].target)
        self.assertEqual(nt.list[0].value, t.list[0].value)

        self.assertTrue(isinstance(nt.list[1], DummyAutoSerializable))
        self.assertEqual(nt.list[1].name, t.list[1].name)
        self.assertEqual(nt.list[1].target, t.list[1].target)
        self.assertEqual(nt.list[1].value, t.list[1].value)

    def test_disabled(self):
        container = DummyContainerSerializable()
        enabled_child = DummyAutoSerializable()
        disabled_child = DummyAutoSerializable()
        disabled_child._serialize = False
        container.children = [enabled_child, disabled_child]
        result = Serializable.deserialize(container.serialize())
        self.assertEqual(len(result.children), 1)
