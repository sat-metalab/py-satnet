# SATNet

## Table of Contents

- [Rationale](#rationale)
- [Requirements](#requirements)
- [Type Hinting](#type-hinting-and-generics)
- [Serialization](#serialization)
- [Usage](#usage)
 - [Server](#creating-a-server)
 - [Client](#creating-a-client)
- [Communication](#communication)
 - [Requests and Responses](#requests-and-responses)
 - [Commands](#commands)
 - [Actions](#actions)
 - [Notifications](#notifications)
- [Contributing](#contributhing-and-development)

## Rationale
SATNet is a networking library that attempts to make it easy to build client/server applications, especially
networked collaborative editors. It also allows the use of different network transport libraries by separating the
client/server API and the library being used to communicate on the network.

As of this writing, two adapters, ZMQ and websockets, are implemented, but it is possible to use them as examples to
implement more adapters in the future.

## Requirements

### Submodules

    git submodule update --init --recursive

### Python, CMake

    sudo apt install cmake python3 python3-pip python3-setuptools
    sudo pip3 install msgpack-python pyzmq aiounittest websockets asynctest

## Type Hinting and Generics
Since this library heavily relies on generics, it uses type hinting in order to do static type checking with
[Mypy](http://mypy-lang.org/). Otherwise it would be pretty difficult to detect bugs in the usage of generics.

To run the type checking use the following:

    ./tools/type-check.sh

## Serialization
Any class that needs to be transfered between the clients and the server must
inherit from `Serializable` and be decorated with the `@serializable` decorator.

Also, in order to be successfully deserialized the class must be registered by
the decorator on both sides. So an import of the class must exists on both the
client and the server.

### Annotated serializable class example

```python
from typing import Optional
from satnet.serializable import Serializable, serializable

# The prefix is used to isolate group of similar classes, the built-in prefixes
# are listed in the `satnet.SerializablePrefix` enum. The id is left to the
# discretion of the class author and should be a value between 0 and 255.
@serializable(prefix=0x00, id=0x00)
class MyClass(Serializable):
    # Fields that should be serialized automatically
    _fields = ['name']

    # init arguments should all be optional since the object witth be
    # initialized before deserialized values are available.
    def __init__(self, name: Optional[str] = None, age: Optional[float] = None) -> None:
        self.name = name
        self.age = age

    def initialize(self) -> None:
        """
        Called after the constructor or after being deserialized in order to use
        the values that came either from the constructor or serialization.
        :return: None
        """
        pass

    def pack(self, write: Callable[[Any], None]) -> None:
        """
        Manually pack the class data (optional).
        If implemented, the `unpack` method must also be implemented.
        :param write: Callable - Method to call to write the next packed value
        :return: None
        """
        super().pack(write)
        write(self.age)

    def unpack(self, read: Callable[[], Any]) -> None:
        """
        Manually unpack the class data (optional).
        If implemented, the `pack` method must also be implemented.
        :param read: Callable - Method to call to read the next packed value
        :return: None
        """
        super().unpack(read)
        self.age = read()
```

### Registering Custom Serializers
Custom serializers can be implemented for special data types. They should extend
the `CustomSerializer[T]` class, where `T` is the type being serialized, be
decorated with a `@serializer` decorator and implement a `serialize` and
`deserialize` method.

Here is the built in custom serializer for serializing `Serializable` objects.
The serializer being built on top of `msgpack`, it handles primitive values
automatically but we have to specify how to handle our own custom classes.

```python
# Serializers should have a unique code that will be used to identify which one
# was used in the serialized data.
@serializer(code=0x00)
class SerializableSerializer(CustomSerializer[Serializable]):
    def serialize(self, obj: Serializable) -> Optional[bytes]:
        # Each serializers are called in turn to find one that supports the
        # value in obj. You should return None if you don't support the data
        # being passed.
        if not isinstance(obj, Serializable):
            return None
        return obj.serialize()

    def deserialize(self, data: bytes) -> Optional[Serializable]:
        return Serializable.deserialize(data)
```


## Usage

### Creating a server
A server is created using this initializer:

```python
def __init__(
    self,
    adapter: ServerAdapter,
    session_class: Optional[Type[T]] = None,
    session_factory: Optional[SessionFactory] = None,
    config: Optional[Dict[str, Any]] = None
) -> None: ...
```

Here is a quick explanation of the different arguments:

|Argument|Type|Description|
|--------|----|-----------|
|adapter|ServerAdapter|Concrete implementation of a `ServerAdapter` that is to be used in order to communicate with the network.|
|session_class|Optional[Type[RemoteClient[T]]]|Concrete implementation of a `RemoteSession` that will be instanced to represent client connections to the server. If not provided then `session_factory` must be used instead.|
|session_factory|SessionFactory (Optional[Callable[[SessionAdapter], T]])|If not using `session_class` this should be a callable that accepts a `SessionAdapter` and returns an instance of a concrete implementation of `RemoteSession`.|
|config|Optional[Dict[str, Any]]|Relevant section of a configuration|

And an example server being created:

```python
import time
from time import sleep
from satnet.adapters.zmq.server import ZMQServer
from satnet.server import Server, RemoteSession

class MyRemoteSession(RemoteSession):
    """Class that will be instantiated for every client connection"""
    pass

server = Server[MyRemoteSession](
    adapter=ZMQServer(),
    session_class=MyRemoteSession
)

server.start(port=9000)

last = time.time()
while True:
    now = time.time()
    dt = now - last
    last = now
    server.step(now, dt)
    sleep(0.001)
```

#### Server Parts

##### Remote Session
The remote session represents a client connection to the server. One is created by the server for each client that
connects by using the `session_class` or calling the `session_factory`. The remote session exists as long as the client
is connected.

The remote session will be passed to the methods of actions/commands/requests/notifications so it is through the session
that messages are sent to the client.


## Creating a client

A client is created using this initializer:

```python
def __init__(
    self,
    session: T,
    adapter: ClientAdapter,
    config: Optional[Dict[str, Any]]
) -> None:
```

Here is a quick explanation of the different arguments:

|Argument|Type|Description|
|--------|----|-----------|
|session|T|Concrete implementation of a `LocalSession` that represents this client connections to the server.|
|adapter|ClientAdapter|Concrete implementation of a `ClientAdapter` that is to be used in order to communicate with the network.|
|config|Optional[Dict[str, Any]]|Relevant section of a configuration|

And an example client being created:

```python
import time
from time import sleep
from typing import Optional
from satnet.adapters.zmq.client import ZMQClient
from satnet.client import Client, LocalSession

class MyLocalSession(LocalSession):
    """Class that will be instantiated for every client connection"""
    pass


class MyClient(Client[MyLocalSession]):
    def __init__(self, config: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(
            session=MyLocalSession(client=self),
            adapter=ZMQClient(),
            config=config
        )

client = MyClient()
client.connect(host="localhost", port=9000)

last = time.time()
while True:
    now = time.time()
    dt = now - last
    last = now
    client.step(now, dt)
    sleep(0.001)
```

#### Client Parts

##### Session
The local session represents the client's connection to the remote server. An instance of `LocalSession` or a subclass
of it is required to initialize the client.

The local session will be passed to the methods of commands/requests/notifications so it is through the session that messages
are sent to the server.


## Communication
Different types of messages can be sent/received by both the client (local session) and server (remote sessions). They
all serve different purposes.

Each message type you create should extend one of the following classes. This allows the writing of the message handling
code to be written in the message class instead of relying on just message ids and registering callbacks manually.  

Quick overview:

|Type|Purpose|
|----|-------|
|[Request](#requests-and-responses)|Used when a response is required.|
|[Command](#commands)|One way instruction telling the other party to do something.|
|[Action](#actions)|Per context undo/redo-able action.|
|[Notification](#notifications)|Similar to commands but with a subscription model.|

### Requests and Responses
A `Request` message is used when a `Response` is required from the other party.
Sending a request will execute the associated response on the resquesting party
once received.

#### Sending a request

##### From the client

```python
client.session.request(Ping())
```

##### From the server

```python
# To a particular remote session's client
session.request(Ping())

# To every clients
server.request_all(Ping())
```

#### Sample annotated request and response:

```python
import time
from typing import Optional
from satnet.request import Request, request, Response, response


# The request decorator registers this class as a serializable request (request
# prefix is added by the decorator). Every request type requires a unique id
# (among requests) to be registered with.
@request(id=0x01)
class Ping(Request[MyRemoteSession]):
    """
    Requests are generic and take a session type parameter so that the session
    argument to `handle` has the right type.
    """

    # Fields that we want serialized. The base class also serializes a request
    # id that is used in the response in order to retrieve the original request
    # when the response is received.
    _fields = ['ping']

    def __init__(self) -> None:
        super().__init__()
        self.ping = time.time()

    def handle(self, session: MyRemoteSession) -> Pong:
        """
        This method will be called by the party receiving the request in order
        to handle it. The session argument will be the local/remote session <T>
        that sent the request, depending on if the requestor is local or remote.

        This method should return a response that will be sent to the requestor.

        :param session: T - Session instance on which to run the request
        :return: Response - Response to the request
        """
        return Pong(self.ping, time.time())


# The response decorator registers this class as a serializable response
# (response prefix is added by the decorator). Every response type requires a
# unique id (among responses) to be registered with.
@response(id=0x01)
class Pong(Response[MyRemoteSession, Ping]):
    """
    Responses are generic and take a session type parameter as well as a type
    parameter of the type of the request that generates this response. This is
    in order to be able to pass the original request to the handle.
    """

    # Fields that we want serialized. The base class also serializes a request
    # id that is used by the requestor in order to retrieve the original request
    # when the response is received.
    _fields = ['ping', 'pong']

    def __init__(self, ping: Optional[float]=None, pong: Optional[float]=None) -> None:
        """
        Since a `Response` is a `Serializable`, arguments must be *optional*
        because we must construct the instance before deserializing the data
        received from the network.
        """
        super().__init__()
        self.ping = ping
        self.pong = pong

    def handle(self, request: Ping, session: MyLocalSession) -> None:
        """
        This method will be called by the party receiving the response in order
        to handle it. The session argument will be the local/remote session <T>
        that sent the request, depending on if the requestor is local or remote.

        The original request that generated this response is also passed as an
        argument in case data from the request is necessary to handle the
        response.

        :param session: T - Session instance with which to handle the response
        :return: Response - Response to the request
        """
        if self.ping is not None and self.pong is not None:
            # Fields must be checked against `None` since this is a
            # `Serializable` and they are *optional*.
            session.editor.clock.update(self.ping, self.pong)
```

### Commands
A `Command` is a one way instruction telling the other to do something. No
confirmation about the success/failure of the command will be provided.

#### Sending a request

##### From the client

```python
client.session.command(DoThis())
```

##### From the server

```python
# To a particular remote session's client
session.command(DoThis())

# To every clients
server.command_all(DoThis())
```

#### Sample annotated command:

```python
from typing import Optional, Tuple
from satnet.command import Command, command

Vec3 = Tuple[float, float, float]


# The command decorator registers this class as a serializable command (command
# prefix is added by the decorator). Every command type requires a unique id
# (among commands) to be registered with.
@command(id=0x01)
class MoveCommand(Command[MyRemoteSession]):
    """ Commands are generic and take a session type parameter. """

    # Fields to serialize with the command.
    _fields = ['obj', 'location']

    def __init__(self, obj: Optional[int] = None, location: Optional[Vec3] = None) -> None:
        """
        Since a `Command` is a `Serializable`, arguments must be *optional*
        since we must construct the instance before deserializing the data
        received from the network.
        """
        super().__init__()
        self.obj = obj
        self.location = location

    def handle(self, session: MyRemoteSession) -> None:
        """
        This method will be called by the party receiving the command in order
        to handle it. The session argument will be the local/remote session <T>
        that sent the request, depending on if the party sending the command is
        the client or the server.

        :param session: T - Session instance on which to run the command
        """
        if self.obj is not None and self.location is not None:
            # Fields must be checked against `None` since this is a
            # `Serializable` and they are *optional*.
            obj = session.get_obj(self.obj)
            obj.location = self.location
```


### Actions
An `Action` is a special type of command that has a revert method, so it can be
undone. Actions will be saved in an action history so that they can be
undone/redone. Actions have an optional context parameter (UUID), it allows the
creation of multiple action histories on the server, one per context id. This
way, different users and/or entities can each have different undo histories.

#### Sending an action
Actions can only be sent from a client to the server.

```python
# In the default context
client.session.action(DoThat())

# In a custom context context
client.session.action(DoThat(context=scene.uuid))
```

#### Undoing/Redoing
Undo and redo commands can be sent to the server.

```python
from satnet.commands.actions import UndoCommand, RedoCommand

# Undo in the default context
client.session.command(UndoCommand())
# Undo with a custom context
client.session.command(UndoCommand(context=scene.uuid))
# Redo in the default context
client.session.command(RedoCommand())
# Redo with a custom context
client.session.command(RedoCommand(context=scene.uuid))
```


#### Sample annotated action:

```python
from typing import Optional
from satnet.action import Action, action


# The action decorator registers this class as a serializable action (action
# prefix is added by the decorator). Every action type requires a unique id
# (among actions) to be registered with.
@action(id=0x01)
class CreateObjectAction(Action[MyRemoteSession]):
    """ Actions are generic and take a session type parameter. """

    # Fields that we want serialized. The base class also serializes a context
    # id that is used by the other party to identify which undo history to
    # retrieve and add this action into.
    _fields = ['type']

    # Local reference to the created object for future reference when reverting
    _object = None

    def __init__(self, context: Optional[int] = 0, type:Optional[str] = None) -> None:
        """
        Since an `Action` is a `Serializable`, arguments must be *optional*
        since we must construct the instance before deserializing the data
        received from the network.
        """
        super().__init__(context=context)
        self.type = type

    def apply(self, session: MyRemoteSession) -> None:
        """
        This method will be called by the party receiving the action in order to
        apply its effect to the session. It could either be called to apply it
        for the first time or to redo an undone (reverted) action.

        :param session: T - Session instance on which to apply the action
        """
        if self._object is None and self.type is not None:
            # Here we verify if we already have the object from a previous
            # application, so that we can reuse it. This is totally  
            # implementation dependant, you can chose to recreate the object
            # every time if it better suits your application and workflow.
            # Otherwise, the fields must also be checked against `None` since
            # this is a `Serializable` and they are *optional*.
            self._object = session.create_object(type=self.type)

        session.add_object(object=self._object)

    def revert(self, session: MyRemoteSession) -> None:
        """
        This method will be called when the action id undone. In this example it
        will save a state in order to be able to more easily re-apply it in the
        future, but this is totally optional and you could chose to completely
        remove the object and then recreate it in every `apply()` call, for
        example.

        :param session: T - Session instance on which to apply the action
        """
        if self._object:
            session.remove_object(object=self._object)
```

Alternatively, it is also possible to send a `Transaction` in place of an
action. A transaction consist of multiple actions that are applied and reverted
in batch. For example:

```python
from satnet.action import Transaction

transaction = Transaction(
    actions=[
        CreateObject(type="Cube"),
        ApplyMaterial(material="brick_wall"),
        MoveObject(location=(1.00, 2.00, 3.00)),
    ]
)
session.action(transaction)
```


### Notifications
Notifications are similar to commands in the sense that they are one-way
messages but contrary to commands they can be subscribed to or unsubscribed from
and they support an optional context (UUID) that allows subscriber to only
subscribe to specific contexts of the same notification.

#### Sending a notification

##### From the client

```python

# Without a context
client.session.notify(SomethingHappened())

# With a context
client.session.notify(SomethingHappened(context=user.uuid))
```

##### From the server

```python
# To a particular remote session's client, without and with a context
session.notify(SomethingHappened())
session.notify(SomethingHappened(context=user.uuid))

# To every clients, without and with a context
server.notify_all(SomethingHappened())
server.notify_all(SomethingHappened(context=user.uuid))

# To every client but one (useful when notifying others about an action of the
# session sending it), without and with a context
session.notify_others(SomethingHappened())
session.notify_others(SomethingHappened(context=user.uuid))
# To every client excluding a list of sessions:
server.notify_all_excluding(notification=SomethingHappened(), exclude=[session])
```

#### Subscribing and unsubscribing
You can subscribe and unsubscribe from notifications in two ways.

##### Default and forced subscriptions
If you want to set up default subscriptions or force a subscription for a
session, you can use the following methods. Subscribing from a local session
will make sure that the server receives the notifications sent from the client
and subscribing from a remote session will make sure the client receives the
notifications sent from the server.

```python
# Will subscribe to all notifications for topic 0x01
session.subscribe(topic=0x01)
# Will subscribe to notifications for topic 0x01 in the context of 123
session.subscribe(topic=0x01, context=123)
# Will unsubscribe to all notifications for topic 0x01
session.unsubscribe(topic=0x01)  
# Will unsubscribe to notifications for topic 0x01 in the context of 123
session.unsubscribe(topic=0x01, context=123)
```

#### Remotely subscribing
Remotely subscribing is done through the use of `Subscribe` and `Unsubscribe`
commands that are sent to the server or client depending on the direction of the
subscription.

```python
from satnet.commands.subscription import SubscribeCommand, UnsubscribeCommand

session.command(SubscribeCommand(topic=0x01, context=123))
session.command(UnsubscribeCommand(topic=0x01, context=123))  
```

#### Sample annotated notification:

```python
from typing import Optional
from satnet.notification import Notification, notification


@notification(id=0x01)
class UserLocation(Notification[MyRemoteSession]):
    _fields = ['location']

    def __init__(self, user: Optional[int] = None, location: Optional[Vec3] = None) -> None:
        """
        Notifications use a context that allows subscribers to filter
        subscriptions on a per-context basis. In this case the context is the
        user that moved, but it can be anything, depending on your needs.
        """
        super().__init__(context=user)
        self.location = location

    def handle(self, session: MyRemoteSession) -> None:
        if self.context is not None and self.location is not None:
            user = session.get_user(self.context)
            user.location = self.location

```

## Contributing and Development
See `CONTRIBUTING.md` for development instructions.
